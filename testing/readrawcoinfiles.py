#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb  4 18:15:00 2020

@author: (c) torsten.leitner@gmail.com
"""

import os
import sys

# own libs from pycoin folder one level above
pycoinpath='../'
sys.path.append(pycoinpath) # add libcoin path here (if not installed as module)

import numpy as np
import pandas as pd
import tables

### OWN libs from within this package
#import libcoin
from libcoin.io.get_rawdataframes_from_iterationfile import get_rawdataframes_from_iterationfile
from libcoin.settings import *

iterationfile='/media/raid/shared/Data/Coin/TEST/0403-LO-03/data59.coin'

print(iterationfile)

dfcoin,dfacc = get_rawdataframes_from_iterationfile(iterationfile,stats={})

