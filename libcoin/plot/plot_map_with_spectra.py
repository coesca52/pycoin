#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec 18 16:22:35 2019

@author: (c) torsten.leitner@gmail.com
"""

#import os
import numpy as np
#import pandas as pd
import matplotlib.pyplot as plt  
#from matplotlib import cm
#from matplotlib.colors import LightSource
#from matplotlib.ticker import LinearLocator, FormatStrFormatter
import matplotlib.ticker as ticker
#import numpy.ma as ma

################################################################################
def plot_map_with_spectra(df,par ,cmap='gist_stern_r',fignum=1
                    ,flag_savefig=True
                    ,picprefix=''
                    ,picpostfix=''
                    ,ax=None,fig=None, notitle=True
                    ,invertXaxis=False
                    ,invertYaxis=False
                    ,framecoords=None,framelinestyle='-',framelinewidth=1
                    , plot_framesums=None
                    , fontsize=10
                    , Ncolorticks=5, Nticks_topspec=5, Nticks_sidespec=4
                    , plot_contourlines=False, contcolors='w',contcolor=10,contalpha=0.7
                    , linplt_sz = 0.4
                    , figsize=None
                    , plot_maxtraces_in_map=False, aspect='auto'
                    , plot_pcolormesh=True
                    , space_between_plots=0
                    , verPlotArea=[0.1,0.9] # (bottom,top)
                    , horPlotArea=[0.1,0.9] # (left,right)
                    ):
################################################################################
    try:
        dfname=df.name
    except:
        dfname=''
    #####
    # energy axis for Auger spectrum
    key=['a1','a2']
    k=key[0]
    x=np.array(df.index)
#    xlim=par[k]['erange']
    xlabel=par[k]['type']+' Energy (eV)'
    
    ##Ecut in PES
    k=key[1] # PES to be summed up
    y=np.array(df.columns)
#    yim=par[k]['erange']
    ylabel=par[k]['type']+' Energy (eV)'
       
    zlabel='counts'
    
    Ncounts=df.sum().sum()
    Z=df.T.values

    # footprint and sumspec for [a1], [a2]
    artof  = 'a1'
    xspec  ={'a1':df.index        , 'a2':df.columns      } 
    yfoot  ={'a1':df.max(axis=1)  , 'a2':df.max(axis=0)  } 
    ysum   ={'a1':df.sum(axis=1)  , 'a2':df.sum(axis=0)  } 
    xspec  ={'a1':df.index        , 'a2':df.columns      } 
    specmax={'a1':ysum['a1'].max(), 'a2':ysum['a2'].max()} 

    ### plotting ...    
    fig, axarr = plt.subplots(nrows=2, ncols=2, sharex='col', sharey='row'
                    , gridspec_kw={'height_ratios':[linplt_sz,1]
                                  ,'width_ratios': [1,linplt_sz]
                                  ,'wspace': space_between_plots
                                  ,'hspace': space_between_plots
                                  ,'left'   :horPlotArea[0], 'right':horPlotArea[1]
                                  ,'bottom' :verPlotArea[0], 'top'  :verPlotArea[1] }
                    , figsize=figsize )

    ### DEFINE LAYOUT:
    # 3x2 grid seen as  colorbar(1x2) + fig(2x2)
    ax ={ 
         'a1'   :axarr[0,0] # top left
        ,'empty':axarr[0,1] # top right
        ,'map'  :axarr[1,0] # bottom left       
        ,'a2'   :axarr[1,1] # bottom right
        }
    ax['empty'].axis('off')

    
    ### ### ###
    # plot Artof a1: Auger spectra (top)
    ### ### ###
    artof='a1'
    ax[artof].plot(xspec[artof], specmax[artof]*ysum[artof]/ysum[artof].max()
            ,'--k',label='sum')
    ax[artof].plot(xspec[artof],specmax[artof]*yfoot[artof]/yfoot[artof].max()
            ,'--r', label='profile (scaled)')
    ax[artof].tick_params(axis='both',which='both',direction='inout'
                   , bottom=True, top=True, left=True, right=True
                   , labelsize=fontsize
                   , labelbottom=False, labeltop=False
                   , labelleft=True, labelright=False    )
    ticks_topspec = ticker.MaxNLocator(nbins=Nticks_topspec,integer=True).tick_values(0,specmax[artof])
    ax[artof].set_yticks(ticks_topspec)
    ax[artof].legend(bbox_to_anchor=(1.04, .7), loc='center left', borderaxespad=0.0)
    
    #'''
    ### ### ###
    # plot Artof a2: PES spectra (side)
    ### ### ###
    artof='a2'
    ax[artof].plot(specmax[artof]*ysum[artof]/ysum[artof].max()  ,xspec[artof] 
            ,'--k',label='sum')
    ax[artof].plot(specmax[artof]*yfoot[artof]/yfoot[artof].max(),xspec[artof] 
            ,'--r', label='profile (scaled)')
    ax[artof].tick_params(axis='both',which='both',direction='inout'
                   , bottom=True, top=True, left=True, right=True
                   , labelsize=fontsize
                   , labelbottom=True, labeltop=False
                   , labelleft=False, labelright=False )
    ticks_sidespec = ticker.MaxNLocator(nbins=Nticks_sidespec,integer=True).tick_values(0,specmax[artof])
    ax[artof].set_xticks(ticks_sidespec)
    #'''
    
    ### ### ###
    ### 2D map
    ### ### ###

    #plot map 
    X,Y=np.meshgrid(x,y)
    if plot_pcolormesh:        
        cx = ax['map'].pcolormesh(X,Y,Z, cmap=cmap, vmin=0) 
    if 0:
        # Plot the test data as a 2D image and the fit as overlaid contours.
        cx = ax['map'].imshow(Z, cmap=cmap, vmin=0
                  ,extent=(x.min(), x.max(), y.min(), y.max())
                  , interpolation='none', aspect=aspect , origin='lower' 
             )
        #X,Y=np.meshgrid(x,y)
    if plot_contourlines:
        ax['map'].contour(X, Y, Z, colors=contcolors,alpha=contalpha, levels=contcolor)
#    ax['map'].set_xlim(xlim)
#    ax['map'].set_ylim(ylim)
    ax['map'].set_xlabel(xlabel)
    ax['map'].set_ylabel(ylabel)
    ax['map'].tick_params(axis='both',which='both',direction='inout'
                   , bottom=True, top=True, left=True, right=True
                   , labelsize=fontsize
                   , labelbottom=True, labeltop=False
                   , labelleft=True, labelright=False )
    if invertYaxis:
        ax['map'].invert_yaxis()
    if invertXaxis:
        ax['map'].invert_xaxis()
    if not notitle:
        ax['map'].set_title(dfname+' counts: '
                       +par[key[1]]['name']
                       +' vs. '
                       +par[key[0]]['name']
                       +', Nc='+str(Ncounts)
                 )
    if framecoords:
        icolor=0
        for ec in framecoords:
            if not 'linecolor' in ec:
                ec['linecolor'] = 'C' + str(icolor)
            icolor+=1
            xr=[ ec['a1range'][0], ec['a1range'][1], ec['a1range'][1]
                , ec['a1range'][0], ec['a1range'][0] ]
            yr=[ ec['a2range'][0], ec['a2range'][0], ec['a2range'][1]
                , ec['a2range'][1], ec['a2range'][0] ]
            ax['map'].plot(xr,yr,color=ec['linecolor']
                           ,linestyle=framelinestyle,linewidth=framelinewidth)
            if plot_framesums:
                ax['a1'].plot(ec['a1'].index ,ec['a1'].values,framelinestyle,label=ec['txt'],color=ec['linecolor'])                
                ax['a2'].plot(ec['a2'].values,ec['a2'].index ,framelinestyle,label=ec['txt'],color=ec['linecolor'])                
    
    if plot_maxtraces_in_map == True:
        
        # plot Auger maxima
        xp=df.index
        yp=df.idxmax(axis=1)
        ax['map'].plot(xp,yp,'-',color='tab:red')

        # plot PES maxima
        xp=df.columns
        yp=df.idxmax(axis=0)
        ax['map'].plot(yp,xp,'-',color='tab:red')

        
    ### ### ###
    ### colorbar
    ###
    if plot_pcolormesh:
         # COLORTICKS ...
        colorticks = ticker.MaxNLocator(nbins=Ncolorticks,integer=True).tick_values(0, np.max(Z))
        # Now adding the colorbar
        # get position, shrink and center it, draw new axes, plot colorbar
        cbpos=ax['empty'].get_position()
        cbx,cbwx = cbpos.y0, (cbpos.x1-cbpos.x0)
        cby,cbwy = cbpos.y0, 0.03
        cbscale = 0.8 ; cbypad = 0.08
        cbx += (cbwx-cbscale*cbwx)/2
        cby += cbypad
        cbaxes = fig.add_axes([cbx,cby,cbscale*cbwx,cbwy])
        cb = plt.colorbar(cx, cax = cbaxes, ticks = colorticks, orientation='horizontal')
        cb.set_label(zlabel, rotation=0, fontsize=fontsize)
    
    #fig.tight_layout()
    
    plt.show()
    if flag_savefig: 
        fig.savefig(picprefix+'_mapNspecs_'+dfname+'_'+par[key[0]]['type']+'-vs-'+par[key[1]]['type']+picpostfix+'.pdf')    
    plt.show()
        
    return ax,fig
###############################################################################
#
#
#
#            






