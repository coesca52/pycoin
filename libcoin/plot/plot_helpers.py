#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jul  7 14:29:10 2019

@author: (c) torsten.leitner@gmail.com
"""

#import os
import numpy as np
#import pandas as pd
import matplotlib.pyplot as plt  
#from matplotlib import cm
import matplotlib.ticker as ticker

################################################################################
################################################################################
def add_frames_to_ax(ax,framecoords,linestyle='-',linewidth=1):
    for ec in framecoords:
        xr=[ ec['a1'][0], ec['a1'][1], ec['a1'][1], ec['a1'][0], ec['a1'][0] ]
        yr=[ ec['a2'][0], ec['a2'][0], ec['a2'][1], ec['a2'][1], ec['a2'][0] ]
        ax.plot(xr,yr,linestyle=linestyle,linewidth=linewidth)
################################################################################
################################################################################
def get_sumenergyspec_from_df(df,nbins=51):
    sumenergy=[]
    sumcounts=[]
    for x in df.index:
        for y in df.columns:
            sumenergy.append(x+y)
            sumcounts.append(df.loc[x,y])
    h=np.histogram(sumenergy,bins=nbins,weights=sumcounts)
    energy,counts = h[1][1::],h[0]
    return energy,counts
################################################################################
################################################################################
def get_diffenergyspec_from_df(df,nbins=51):
    diffenergy=[]
    diffcounts=[]
    for x in df.index:
        for y in df.columns:
            diffenergy.append(x-y)
            diffcounts.append(df.loc[x,y])
    h=np.histogram(diffenergy,bins=nbins,weights=diffcounts)
    energy,counts = h[1][1::],h[0]
    return energy,counts
################################################################################
