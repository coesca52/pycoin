#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jul  7 00:23:20 2019

@author: (c) torsten.leitner@gmail.com
"""

#import os
import numpy as np
#import pandas as pd
import matplotlib.pyplot as plt  
#from matplotlib import cm
#from matplotlib.colors import LightSource
#from matplotlib.ticker import LinearLocator, FormatStrFormatter
import matplotlib.ticker as ticker
import numpy.ma as ma

################################################################################
def plot_pcolor_map(df,par ,cmap='terrain_r',fignum=1
                    ,flag_savefig=False
                    ,picprefix=''
                    ,picpostfix=''
                    ,ax=None,fig=None, notitle=False
                    ,print_dxy=False
                    ,invertXaxis=False
                    ,invertYaxis=False
                    ,framecoords=None,framelinestyle='-',framelinewidth=1
                    ,Ncolorticks = 5
                    ):
################################################################################
#if 1:
    try:
        dfname=df.name
#        print(dfname)
    except:
        dfname=''
    #####
    # energy axis for Auger spectrum
    key=['a1','a2']
    k=key[0]
    x=np.array(df.index)
#    xlim=par[k]['erange']
    xlabel=par[k]['type']+' Energy (eV)'
    
    ##Ecut in PES
    k=key[1] # PES to be summed up
    y=np.array(df.columns)
#    yim=par[k]['erange']
    ylabel=par[k]['type']+' Energy (eV)'
       
    zlabel='counts'
        
    Z=df.T.values
    Ncounts=np.int(Z.sum().sum())
       
    if fig==None:
        fig = plt.figure(fignum)
        fig.clf()
    if ax==None:
        ax=fig.gca()
        
    colorticks = ticker.MaxNLocator(nbins=Ncolorticks).tick_values(0, np.max(Z) )

    cf = ax.pcolormesh(x,y,Z, cmap=cmap, vmin=0) 
    
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    
    if invertYaxis:
        ax.invert_yaxis()
    if invertXaxis:
        ax.invert_xaxis()
        
    if not notitle:
        ax.set_title(dfname+' counts: '
                       +par[key[1]]['name']
                       +' vs. '
                       +par[key[0]]['name']
                       +', Nc='+str(Ncounts)         )
    cb=fig.colorbar(cf, ax=ax, ticks = colorticks )
    cb.set_label(zlabel)
 
    if print_dxy==True:
        ax.text(1.2, -0.06,  'dx='+str(par[key[0]]['de'])+' eV'
                            +'\n'
                            +'dy='+str(par[key[1]]['de'])+' eV'
                 , fontsize=10, verticalalignment='top', transform=ax.transAxes #,bbox=props
                )
        
        
    if framecoords:
        icolor=0
        for ec in framecoords:
            if not 'linecolor' in ec:
                ec['linecolor'] = 'C' + str(icolor)
            icolor+=1
            xr=[ ec['a1range'][0], ec['a1range'][1], ec['a1range'][1], ec['a1range'][0], ec['a1range'][0] ]
            yr=[ ec['a2range'][0], ec['a2range'][0], ec['a2range'][1], ec['a2range'][1], ec['a2range'][0] ]
            ax.plot(xr,yr,color=ec['linecolor'],linestyle=framelinestyle,linewidth=framelinewidth)
        
        
    plt.show()
    if flag_savefig: 
        fig.savefig(picprefix+'_map_'+dfname+'_'+par[key[0]]['type']+'-vs-'+par[key[1]]['type']+picpostfix+'.pdf')    
    plt.show()
    
    par={}
    
    return ax,fig
###############################################################################
#
#
#
#            
