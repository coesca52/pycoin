#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jul  7 14:30:11 2019

@author: (c) torsten.leitner@gmail.com
"""
import os
import numpy as np
#import pandas as pd
import matplotlib.pyplot as plt  
#from matplotlib import cm
import matplotlib.ticker as ticker

from libcoin.plot.plot_helpers import *
from libcoin.plot.plot_pcolor_map import plot_pcolor_map

################################################################################
def plot_allinone_collage(df, ecut, par, fignum=1, nbinsSumDiff=51, flag_savefig=True
                        , EsumLines=None  , EsumLinesColor ='g',EsumLineWidth =0.5 
                        , EdiffLines=None , EdiffLinesColor='b',EdiffLineWidth=0.5  
                        , framelinewidth=1,framelinestyle='-'
                        , invertA1axis=False,invertA2axis=False):

    try:
        plt.close(fignum)
    except:
        pass
    fig = plt.figure( fignum, figsize=(8. , 11.69) )
    fig.clf()
    

    # define dictionary with all axes of collage
    nspx,nspy=3,2
    axdict={}
    axdict['a1']      = plt.subplot(nspx,nspy,1) ; axdict['a1'].set_title('Detector A1: '+par['a1']['type'])
    axdict['a2']      = plt.subplot(nspx,nspy,2) ; axdict['a2'].set_title('Detector A2: '+par['a2']['type'])
    axdict['sum']     = plt.subplot(nspx,nspy,3) ; axdict['sum'].set_title('Energy Sum')
    axdict['diff']    = plt.subplot(nspx,nspy,4) ; axdict['diff'].set_title('Energy Difference')
    # resize chart area and plot legend aside
    chartheightscale=0.78
    chartwidthscale =0.83
    for k,v in axdict.items():
        chartBox = v.get_position()
        v.set_position([chartBox.x0+0.0, chartBox.y0+0.05, chartwidthscale*chartBox.width, chartheightscale*chartBox.height])
    
    nspx,nspy,pos=3,1,3
    axdict['map']   = plt.subplot(nspx,nspy,pos)
    
    if invertA1axis:
        axdict['a1' ].invert_xaxis()
        axdict['map'].invert_xaxis()
    if invertA2axis:
        axdict['map'].invert_yaxis()
        axdict['a2' ].invert_xaxis()
  
    # draw map
    ax=axdict['map']
    ax,fig = plot_pcolor_map(df,par ,cmap='terrain_r',ax=axdict['map'],fig=fig, notitle=True)
    # resize chart area and plot legend aside
#        chartBox = ax.get_position()
#        ax.set_position([chartBox.x0, 0.00+chartBox.y0, 1*chartBox.width, 1*chartBox.height])
    # draw frames over map
    add_frames_to_ax(ax=ax,framecoords=ecut,linestyle=framelinestyle,linewidth=framelinewidth)

    # draw E1+E2=Esumline into map
    if EsumLines:
        for esum in EsumLines:
            xlim=ax.get_xlim() ; ylim=ax.get_ylim()
            xx,yy = df.index,esum-df.index
            ax.plot(xx,yy,'-',color=EsumLinesColor,linewidth=EsumLineWidth)
            ax.set_xlim(xlim)  ; ax.set_ylim(ylim)
            ytxt = ylim[1]
            xtxt = xx[  np.argmin( np.abs(yy-ytxt) ) ]
            ax.text(xtxt,ytxt,str(esum),color=EsumLinesColor
                    ,rotation='vertical',fontsize=7,va='center',ha='center'
                    ,bbox=dict(boxstyle='round', facecolor='white', alpha=1, linewidth=0.5))
    # draw E1-E2=Ediffline into map
    if EdiffLines:
        for ediff in EdiffLines:
            xlim=ax.get_xlim() ; ylim=ax.get_ylim()
            xx,yy = df.index,df.index-ediff
            ax.plot(xx,yy,'-',color=EdiffLinesColor,linewidth=EdiffLineWidth)
            ax.set_xlim(xlim)  ; ax.set_ylim(ylim)
            ytxt = ylim[0]
            xtxt = xx[  np.argmin( np.abs(yy-ytxt) ) ]
            ax.text(xtxt,ytxt,str(ediff),color=EdiffLinesColor
                    ,rotation='vertical',fontsize=7,va='bottom',ha='center'
                    ,bbox=dict(boxstyle='round', facecolor='white', alpha=1, linewidth=0.5))
    #
    
    # draw A1 & A2 specs
    axdict['a1'].plot(df.index  ,df.sum(axis=1),':k',label='fullspec')
    axdict['a2'].plot(df.columns,df.sum(axis=0),':k',label='fullspec')
    for key in ['a1','a2']:
        for ec in ecut:
            axdict[key].plot( ec[key+'energy'], ec[key+'counts'],label=ec['txt'] )
#        ax[key].legend()
        axdict[key].set_xlabel(par[key]['name']+'  E (eV)')
        axdict[key].set_ylabel('counts')
#        ax[key].set_title('a')


    
    # draw Sum specs
    key='sum' ; ax=axdict[key]
    nbins=nbinsSumDiff
    scale=0.5
    energy,counts = get_sumenergyspec_from_df(df,nbins=nbins)
    ax.plot(energy,scale*counts,':k',label='fullspec')        
    for ec in ecut:
        energy,counts = get_sumenergyspec_from_df(ec['df'],nbins=nbins)
        ax.plot(energy,counts,'-',label=ec['txt']) 
        ax.set_xlabel('E1 + E2 (eV)')
        ax.set_ylabel('counts')
    # plot Esum as vertical line to sumspecs
    if EsumLines:
        for esum in EsumLines:
            ylim=ax.get_ylim()
            ax.plot([esum,esum],ylim,'-',color=EsumLinesColor,linewidth=EsumLineWidth)
            ax.set_ylim(ylim)
            

    
    # draw Diff specs
    key='diff' ; ax=axdict[key]
    nbins=nbinsSumDiff
    scale=0.5
    energy,counts = get_diffenergyspec_from_df(df,nbins=nbins)
    ax.plot(energy,scale*counts,':k',label='fullspec')        
    for ec in ecut:
        energy,counts = get_diffenergyspec_from_df(ec['df'],nbins=nbins)
        ax.plot(energy,counts,'-',label=ec['txt']) 
        ax.set_xlabel('E1 - E2 (eV)')
        ax.set_ylabel('counts')
    # plot Esum as vertical line to sumspecs
    if EdiffLines:
        for ediff in EdiffLines:
            ylim=ax.get_ylim()
            ax.plot([ediff,ediff],ylim,'-',color=EdiffLinesColor,linewidth=EdiffLineWidth)
            ax.set_ylim(ylim)





    # supertitle to all subplots    
    plt.suptitle(par['prefix']+', '+df.name+', hv = '+str(par['Ephot'])+' eV')
    
    
    
    plt.show()
    
    if flag_savefig: 
        fig.savefig(os.path.join(par['picpath'],par['prefix']+'_allinone_'+df.name+'.pdf')  )
    
    plt.show()

 
