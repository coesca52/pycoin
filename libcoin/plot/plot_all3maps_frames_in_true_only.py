#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jul  7 00:23:20 2019

file: plot_all3maps_frames_in_true_only.py

@author: (c) torsten.leitner@gmail.com
"""

#import os
import numpy as np
import matplotlib.pyplot as plt  
#from matplotlib import cm
#from matplotlib.colors import LightSource
#from matplotlib.ticker import LinearLocator, FormatStrFormatter
import matplotlib.ticker as ticker
import numpy.ma as ma

################################################################################
def plot_all3maps_frames_in_true_only(dfcoin,dfacc,dftrue
                    ,par ,cmap='terrain_r',fignum=1
                    ,flag_savefig=False
                    ,picprefix=''
                    ,picpostfix=''
                    ,notitle=False
                    ,print_dxy=False
                    ,invertXaxis=False
                    ,invertYaxis=False
                    ,framecoords=None,framelinestyle='-',framelinewidth=1
                    ,figsize=(15, 5)
                    ,Ncolorticks = 5
                    ):
    
    fig, axes = plt.subplots(1, 3, figsize=figsize) ;    iax=-1
    for df in [dfcoin,dfacc,dftrue]:
        iax+=1
        ################################################################################
        lowerthreshold=0
        try:
            dfname=df.name
        except:
            dfname=''
        #####
        # energy axis for Auger spectrum
        key=['a1','a2']
        k=key[0]
        x=np.array(df.index)
    #    xlim=par[k]['erange']
        xlabel=par[k]['type']+' Energy (eV)'

        ##Ecut in PES
        k=key[1] # PES to be summed up
        y=np.array(df.columns)
    #    yim=par[k]['erange']
        ylabel=par[k]['type']+' Energy (eV)'

        zlabel='counts'

        # data
        Z=df.T.values
        Ncounts=np.int(Z.sum().sum())

        # the figure
        ax=axes[iax]

        colorticks = ticker.MaxNLocator(nbins=Ncolorticks).tick_values(np.min(Z), np.max(Z) )
        
        cf = ax.pcolormesh(x,y,Z, cmap=cmap, vmin=0) # 
        
        ax.set_xlabel(xlabel)
        ax.set_ylabel(ylabel)

        if invertYaxis:
            ax.invert_yaxis()
        if invertXaxis:
            ax.invert_xaxis()


        if not notitle:
            ax.set_title(dfname+' counts: '
                           +par[key[1]]['name']
                           +' vs. '
                           +par[key[0]]['name']
                           +', Nc='+str(Ncounts)
                     )
        cb=fig.colorbar(cf, ax=ax, ticks = colorticks )
        cb.set_label(zlabel)

        if print_dxy==True:
            ax.text(1.2, -0.06,  'dx='+str(par[key[0]]['de'])+' eV'
                                +'\n'
                                +'dy='+str(par[key[1]]['de'])+' eV'
                     , fontsize=10, verticalalignment='top', transform=ax.transAxes #,bbox=props
                    )


        if dfname=='true':
            if framecoords:
                icolor=0
                for ec in framecoords:
                    if not 'linecolor' in ec:
                        ec['linecolor'] = 'C' + str(icolor)
                    icolor+=1
                    xr=[ ec['a1range'][0], ec['a1range'][1], ec['a1range'][1], ec['a1range'][0], ec['a1range'][0] ]
                    yr=[ ec['a2range'][0], ec['a2range'][0], ec['a2range'][1], ec['a2range'][1], ec['a2range'][0] ]
                    ax.plot(xr,yr,color=ec['linecolor'],linestyle=framelinestyle,linewidth=framelinewidth)


       
    ###############################################################################

    fig.tight_layout()
    
    
    if flag_savefig:
        fig.savefig(picprefix+'_3maps_'+par[key[0]]['type']+'-vs-'+par[key[1]]['type']+picpostfix+'.pdf')    
    
    plt.show()
    
    return ax,fig
###############################################################################
#
#
#
#            
