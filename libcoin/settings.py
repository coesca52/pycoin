#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 14 13:17:28 2019

@author: (c) torsten.leitner@gmail.com
"""

import numpy as np

# groupmask

###########################################################################
#TDC channel map, counted from 0
###########################################################################
TDCchannels = { 'A1':{'x1': 0,'x2': 1,'y1': 2,'y2': 3,'mcp': 7} 
               ,'A2':{'x1':21,'x2':22,'y1':23,'y2':24,'mcp':28}
               ,'trig':6    }
mask_trig = 2**TDCchannels['trig']
###########################################################################
mask_a1   = sum([(2**v) for k,v in TDCchannels['A1'].items()]) + mask_trig
mask_a2   = sum([(2**v) for k,v in TDCchannels['A2'].items()]) + mask_trig
mask_coin = mask_a1 | mask_a2
###########################################################################
    

###########################################################################
# Pulse Separationnn Time, 800ns at BESSY
PulseSepTime=800000 #ps
###########################################################################
# fullrange TOF range ...
a1tofrange=np.array([0,PulseSepTime])#ps
a2tofrange=np.array([0,PulseSepTime])#ps
###########################################################################
# range for RAW times from x1x2y1y2 delayline signals
xrawrange=np.array([-37500,37500]) # ps
yrawrange=np.array([-37500,37500]) # ps
###########################################################################


# ASC config files
###########################################################################
cfgfilelist = ['acquisition.cfg']#,'detector.txt','elements.txt','timing.txt','extra.txt']
###########################################################################




