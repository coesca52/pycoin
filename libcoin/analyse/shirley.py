#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Dec 14 00:21:51 2019

@author: (c) torsten.leitner@gmail.com
"""
import numpy as np
import pandas as pd

### ### ### ### ### ### ### ### ### ### ### ###
def funcShirleyBG(y,BGscale,Offset):
    out  = y[::-1].cumsum()[::-1]  #reverse cumsum 
    out -= out.min()
    out /= out.max()
    return BGscale*out + Offset
### ### ### ### ### ### ### ### ### ### ### ###
def guessShirleyBG(y,edgesize=5):
    left = y[0:edgesize-1].mean()
    right= y[-edgesize:-1].mean()
    #
    Offset = min(left,right)
    BGscale= max(left,right) - Offset
    return BGscale,Offset
### ### ### ### ### ### ### ### ### ### ### ###



########################################################################
def calc_shirley_2d(dfin,edgesize=7):
    df=dfin.copy()
    # copy pure data as np.array
    data=df.to_numpy()
    x=np.array(df.index)
    y=np.array(df.columns)
    tmp = 0*data.copy()
    bg  = 0*data.T.copy()
    # Shirley along X  (Auger)
    # loop over all x- values and calc Y-Shirley
    for i in range(data.shape[0]):
        BGscale,Offset=guessShirleyBG(data[i,:],edgesize=edgesize) 
        if Offset<0 : Offset=0
        tmp[i,:] = funcShirleyBG(data[i,:],BGscale,Offset)
    # Shirley along Y  (PES)
    # loop over all y- values and calc X-Shirley
    tmp-=data ; tmp*=-1 ; tmp=tmp.T
    for i in range(tmp.shape[0]):
        BGscale,Offset=guessShirleyBG(tmp[i,:],edgesize=edgesize) 
        if Offset<0 : Offset=0
        bg[i,:] = funcShirleyBG(tmp[i,:],BGscale,Offset)
    dfBG=pd.DataFrame(index=x,columns=y,data=bg.T)
    return dfBG
########################################################################











