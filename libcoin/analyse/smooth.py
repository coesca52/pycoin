#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Dec 14 01:02:25 2019

@author: (c) torsten.leitner@gmail.com
# -*- coding: utf-8 -*-
#
# (c) 2019 torsten.leitner@gmail.com
#
##########################################
various helpers for pycoin:
##########################################
functions:

out = moving_average_2d(dfin , span=5, spany=None)
    - ...
    
out = smooth_with_gaussiankernel(Z,sigma=5)
    - Smoothing of datapoints with a gaussian kernel.
      Wrapper for:  scipy.ndimage.gaussian_filter()
    Vars:
        y        -  datapoints
        sigma    -  width of smoothing gaussian in units gripdpoints
                can be n-dimensional for different sigmas in n dims.
##########################################
"""

import numpy as np
import pandas as pd

########################################################################
# moving average, no binning
########################################################################
def moving_average_2d(dfin , span=5, spany=None):
    # span: mean(i+/-span) ; if spany not given spany=span
    #
    df=dfin.copy()
    if not spany: spany=span
    spanx=span
    # copy pure data as np.array
    data=df.to_numpy()
    x=np.array(df.index)
    y=np.array(df.columns)
    tmp = 0*data.copy()
    tmp=0*data.copy()
    avg=0*data.copy()
    for i in range(tmp.shape[0]):
        i1=i-spanx ; i2=i+spanx
        if i1<0: 
            i1=0 
        tmp[i,:] = data[ i1:i2 , : ].mean(axis=0)
    for i in range(tmp.shape[1]):
        i1=i-spany ; i2=i+spany
        if i1<0: 
            i1=0 
        avg[:,i] = tmp[ : , i1:i2 ].mean(axis=1)
    dfavg=pd.DataFrame(index=x,columns=y,data=avg)
    return dfavg
########################################################################





########################################################################
# n-Dimensional smoothing filter with gaussian kernel
# wrapper for:  scipy.ndimage.gaussian_filter
########################################################################
def smooth_with_gaussiankernel(Z,sigma=5):
    '''
Smoothing of datapoints with a gaussian kernel.
Wrapper fucntion for:  scipy.ndimage.gaussian_filter(Z,sigma)

Vars:
    y        -  datapoints
    sigma    -  width of smoothing gaussian in units gripdpoints
                can be n-dimensional for different sigmas in n dims.

Output
    Z_smoothed = scipy.ndimage.smooth_gaussiankernel(Z,sigma=5)    
    '''
    from scipy.ndimage import gaussian_filter
    return gaussian_filter(Z, sigma=sigma)
########################################################################




