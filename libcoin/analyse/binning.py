#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Dec 14 01:34:19 2019

@author: (c) torsten.leitner@gmail.com
"""
import numpy as np
import pandas as pd

########################################################################
# integer bin data
########################################################################
def bin2d_by_averaging_fixed_span(dfin , span=5, spany=None):
    '''
    bin 2d dataframe data by averaging over a fixed number of points
    '''
    df=dfin.copy()   
    if not spany: spany=span
    spanx=span
    data=df.to_numpy()
    x=np.array(df.index)
    y=np.array(df.columns)
    nxb,nyb = int(len(x)/span)+1 , int(len(y)/span)+1
    # new axes
    xbin=np.zeros(nxb)
    ybin=np.zeros(nyb)
    for i in range(nxb): 
        xbin[i] = x[ spanx*i : spanx*(i+1) ].mean() # average every span elements
    for i in range(nyb): 
        ybin[i] = y[ spany*i : spany*(i+1) ].mean() # average every span elements
    # bin the data
    binned=np.zeros( (nxb,nyb) )
    tmp=np.zeros( (data.shape[0],nyb)  )
    for i in range(nyb):
        tmp[:,i]    = data[ : , spany*i : spany*(i+1) ].mean(axis=1)
    for i in range(nxb): 
        binned[i,:] = tmp [ spanx*i : spanx*(i+1) , : ].mean(axis=0) 
    return pd.DataFrame(index=xbin,columns=ybin,data=binned)
########################################################################


