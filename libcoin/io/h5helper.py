#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 19 15:56:41 2019

Helper functions to deal with libcoinn h5 files


@author: (c) torsten.leitner@gmail.com
"""

def delete_table_from_h5(h5filenname, key, h5path='/'):
        import h5py
        with h5py.File(h5filenname, 'a') as h5file:
            try: 
                del h5file[h5path+key]
            except:
                pass