#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 20 03:32:15 2019

@author: (c) torsten.leitner@gmail.com
"""


import numpy as np
import pandas as pd

### OWN libs


### define pytables table
from libcoin.io.datastructures import rawdataformat
from libcoin.io.datastructures import tdcEventdataformat
###

# settings settings.py  => move to coinsettingns.ini
from libcoin.settings import *



###############################################################################
def get_rawdataframes_from_iterationfile(iterationfile,stats={}):
    '''
    read raw data from coin_xxx.dat iterationn files and 
        return dfcoin, dfacc and stats
    '''
    with open(  iterationfile ,'rb') as f:
        tdcraw  = np.fromfile(f, dtype=rawdataformat)
        
        
    tdc=np.array( tdcraw, dtype=tdcEventdataformat )
    
#    print('\n len ',len(tdc))
    itertime =1e-12*(tdc['trig'][-1]-tdc['trig'][0])
    timestamp=1e-12*tdc['trig'][-1]
        
#    tdc['t1']   = np.float64(tdc['t1'])
#    tdc['t2']   = np.float64(tdc['t2'])
#    tdc['trig'] = np.float64(tdc['trig'])
        
        
    tdc['t1']=tdc['t1']-tdc['trig'] 
    tdc['t2']=tdc['t2']-tdc['trig']


     
    # trigID within iteration
    trigID=np.rint((tdc['trig']-tdc['trig'][0])/PulseSepTime)
    # trigID since start of TDC copunter / detector server
    #trigID=np.rint((tdc['trig'])/PulseSepTime)
    
    # bitfield / index where trigID==1
    jdtrigID=( np.append(0,np.diff(trigID)) == 1 )
    
    
    mask=np.zeros(len(tdc),dtype=np.int)
    mask[ (mask_a1&tdc['mask']==mask_a1) ]=1
    mask[ (mask_a2&tdc['mask']==mask_a2) ]=2
    mask[ (tdc['mask']==mask_coin) ]=3
    
    # summ all mcp counts
    NmcpA1=len( tdc['t1'][(tdc['t1']>=0) & (tdc['t1']<=PulseSepTime)] )
    NmcpA2=len( tdc['t2'][(tdc['t2']>=0) & (tdc['t2']<=PulseSepTime)] )
                
    # sum single events in tofrange 
    NsinglesA1=len( tdc['t1'][ (mask_a1&tdc['mask']==mask_a1) & (tdc['t1']>a1tofrange[0]) & (tdc['t1']<a1tofrange[1])] )
    NsinglesA2=len( tdc['t2'][ (mask_a2&tdc['mask']==mask_a2) & (tdc['t2']>a2tofrange[0]) & (tdc['t2']<a2tofrange[1])] )


    
    # Convert np.struct array to DataFrame
    dftdc=pd.DataFrame(tdc)
    # add mnask to DataFrame
    dftdc['mask']=mask
    
    # coins from dftdc
    dfcoin=dftdc[ (dftdc['mask']==3)
        & (dftdc['t1']>a1tofrange[0]) & (dftdc['t1']<a1tofrange[1])
        & (dftdc['t2']>a2tofrange[0]) & (dftdc['t2']<a2tofrange[1])    
        ].copy()
    
    C1=len(dfcoin)
    print(C1)
    
    # shifted coins                     
    # shift by ... pulses
    shift=-1


    # create acc dataset: 1 vs shift(2)
    df12=dftdc.copy()
    df12.x2 = np.roll( df12.x2 , shift)
    df12.y2 = np.roll( df12.y2 , shift)
    df12.t2 = np.roll( df12.t2 , shift)                
    # select dataset: range and all combinations of hits
    df12 = df12[ jdtrigID
        &  (   ( (df12['mask']==1)&(df12['mask'].shift(-1)==2) ) 
             | ( (df12['mask']==1)&(df12['mask'].shift(-1)==3) )
             | ( (df12['mask']==3)&(df12['mask'].shift(-1)==2) )    
             | ( (df12['mask']==3)&(df12['mask'].shift(-1)==3) )    
           ) 
        & (df12['t1']>a1tofrange[0]) & (df12['t1']<a1tofrange[1])
        & (df12['t2']>a2tofrange[0]) & (df12['t2']<a2tofrange[1]) 
        ].copy()
    #C12=len(df12)


    # create acc dataset: shift(1) vs 2
    df21=dftdc.copy()
    df21.x1 = np.roll( df21.x1 , shift)
    df21.y1 = np.roll( df21.y1 , shift)
    df21.t1 = np.roll( df21.t1 , shift)
  
    df21 = df21[ jdtrigID
        &  (   ( (df21['mask']==2)&(df21['mask'].shift(-1)==1) ) 
             | ( (df21['mask']==2)&(df21['mask'].shift(-1)==3) )
             | ( (df21['mask']==3)&(df21['mask'].shift(-1)==1) )    
             | ( (df21['mask']==3)&(df21['mask'].shift(-1)==3) )    
           ) 
        & (df21['t1']>a1tofrange[0]) & (df21['t1']<a1tofrange[1])
        & (df21['t2']>a2tofrange[0]) & (df21['t2']<a2tofrange[1]) 
        ].copy()

    # merge the two lists and sort them by trigger time
    dfacc = df12.append(df21).sort_values(by=['trig'], axis=0, ascending=True
                       , kind='mergesort', na_position='last'
                       ).copy()
    C2    = len(dfacc)
    
    
    dfcoin = dfcoin.drop(columns=['mask'])
    dfacc  =  dfacc.drop(columns=['mask'])
    
    
    #df astype here
#    dfcoin['t1']  =dfcoin['t1'].astype('f8').copy()
#    dfcoin['t2']  =dfcoin['t2'].astype('f8').copy()
#    dfcoin['trig']=dfcoin['trig'].astype('f8').copy()
#    
    # astype doesnt work as wanted... 
    # try to initialise new dataframe from new np.stgructured array with correct dataforamt
    
    
        
    '''
    ###########################################
    # statistics per iteration
    ###########################################
    stats['C1'].append(C1)
    stats['C2'].append(C2)
    stats['timestamp'].append(timestamp)
    stats['itertime'].append(itertime)
    stats['NmcpA1'].append(NmcpA1)
    stats['NmcpA2'].append(NmcpA2)
    stats['NsinglesA1'].append(NsinglesA1)
    stats['NsinglesA2'].append(NsinglesA2)
    #'''        
    
    return dfcoin,dfacc,#stats
###############################################################################


