#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 19 15:31:32 2019

@author: (c) torsten.leitner@gmail.com
"""

### define pytables table
import numpy as np
import tables


### define format of raw TDCdata in binary file
rawdataformat=np.dtype([ ('x1','int32'),('y1','int32'),('t1','uint64')
                        ,('x2','int32'),('y2','int32'),('t2','uint64')
                        ,('trig','uint64'),('mask','uint64') ]  )
### define format of raw TDCdata in h5file
tdcEventdataformat=np.dtype([ ('x1','int32'),('y1','int32'),('t1','f8')
                             ,('x2','int32'),('y2','int32'),('t2','f8')
                             ,('trig','f8'),('mask','uint64') ]    )
class tdcEvent(tables.IsDescription):
    x1   = tables.Int32Col(dflt=0,  pos = 1)  
    y1   = tables.Int32Col(dflt=0,  pos = 2)  
    t1   = tables.Float64Col(dflt=0, pos = 3)
#    t1   = tables.UInt64Col(dflt=0, pos = 3)
    x2   = tables.Int32Col(dflt=0,  pos = 4)  
    y2   = tables.Int32Col(dflt=0,  pos = 5)  
    t2   = tables.Float64Col(dflt=0, pos = 6)
#    t2   = tables.UInt64Col(dflt=0, pos = 6)
    trig = tables.Float64Col(dflt=0, pos = 7) 
#    trig = tables.UInt64Col(dflt=0, pos = 7) 



### define format of converted etp (Ekin/Theta/Phi) in h5file
etpdataformat=np.dtype( [ ('E1', 'f8'), ('theta1', 'f8'), ('phi1', 'f8')
                         ,('E2', 'f8'), ('theta2', 'f8'), ('phi2', 'f8')
                         ,('trig','f8') ]   ) 
class etpEvent(tables.IsDescription):
    E1       = tables.Float64Col(dflt=0,  pos = 1)  
    theta1   = tables.Float64Col(dflt=0,  pos = 2)  
    phi1     = tables.Float64Col(dflt=0,  pos = 3)
    E2       = tables.Float64Col(dflt=0,  pos = 4)  
    theta2   = tables.Float64Col(dflt=0,  pos = 5)  
    phi2     = tables.Float64Col(dflt=0,  pos = 6)
    trig     = tables.Float64Col(dflt=0,  pos = 7)
#        gamma    = tables.Float64Col(dflt=0,  pos = 8)
