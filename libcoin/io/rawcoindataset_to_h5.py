#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 14 13:26:41 2019

@author: (c) torsten.leitner@gmail.com
"""



import numpy as np
import os
import pandas as pd
import tables
#"""
### OWN libs from within this package
#import libcoin
from libcoin.tools.progressbar import progressbar
from libcoin.io.ASCcfg import ASCcfg_from_text
from libcoin.io.ASCcfg import getImageTrafoFromASCcfg
from libcoin.io.ASCcfg import getEnergyTrafoFromASCcfg
from libcoin.io.ASCcfg import ASCcfg_to_h5
from libcoin.io.get_rawdataframes_from_iterationfile import get_rawdataframes_from_iterationfile
from libcoin.io.get_etpdataframe_from_rawdataframe import get_etpdataframe_from_rawdataframe
#
#### define pytables table
from libcoin.io.datastructures import tdcEvent
from libcoin.io.datastructures import etpEvent
####
## settings settings.py  => move to coinsettingns.ini
from libcoin.settings import *

#"""

###############################################################################
###############################################################################
###############################################################################
def multiple_rawcoindatasets_to_h5( datafolderlist,fnamebase
                ,fileext='.coin'
                ,outpath=None # if outpath=None outfile is created next to datafolder
                ,startiteration=1,stopiteration=-1
                ,a1tofrange=[0,800000],a2tofrange=[0,800000]
                ,PulseSepTime=800000
                ,xrange=[-37500,37500],yrange=[-37500,37500]
                ,mode='etp+raw_to_ana'
                ,raw_postfix='_xytraw'
                ,ana_postfix='_analysed'
                ,ps_to_expand_TOFrange_for_raw_ana=100e3 #ps
                ,thetamax_degree=20):
#    pass
#"""
    #######################################
    # loop over many datafolders ...
    #######################################
    import time
    print()
    for datafolder in datafolderlist:   
       #######################################
        tick=time.time()
        print('--- --- --- --- --- --- --- ---')
        print('raw data folder: \t',datafolder)
        if outpath==None: 
            h5outpath = os.path.normpath( os.path.join(datafolder,'..') )
        else:
            h5outpath=outpath
        print('outpath: \t\t',outpath)    
        #result= 
        rawcoindataset_to_h5(  datafolder,fnamebase,fileext,h5outpath
                ,startiteration=startiteration,stopiteration=stopiteration
                ,a1tofrange=a1tofrange,a2tofrange=a2tofrange
                ,PulseSepTime=PulseSepTime
                ,xrange=xrange,yrange=yrange
                ,mode=mode
                ,raw_postfix=raw_postfix
                ,ana_postfix=ana_postfix
                ,ps_to_expand_TOFrange_for_raw_ana=ps_to_expand_TOFrange_for_raw_ana #ps
                ,thetamax_degree=thetamax_degree
                )
        tock=time.time()    
        print('time for converting:  {:2.2} s'.format(tock-tick) )
        print('--- --- --- --- --- --- --- ---')
        print()
###############################################################################
###############################################################################
###############################################################################




###############################################################################
###############################################################################
###############################################################################
def rawcoindataset_to_h5( 
         datapath,fnamebase,fileext
        ,outpath='./'
        ,startiteration=0,stopiteration=-1
        ,a1tofrange=[0,800000],a2tofrange=[0,800000]
        ,PulseSepTime=800000
        ,xrange=[-37500,37500],yrange=[-37500,37500]
        ,mode='raw_only'
        ,raw_postfix='_xytraw'
        ,ana_postfix='_analysed'
        ,ps_to_expand_TOFrange_for_raw_ana=50000 #ps
        ,thetamax_degree=20
        ):
    '''
    modes: 'raw_only' , 'etp+raw_to_ana' , 'raw_to_ana',  'etp_to_ana'
    '''

    flag_create_ana_file  = False
    flag_create_etp_table = False
    flag_save_raw_to_ana  = False
    flag_save_etp_to_ana  = False
    
    # set flags depending on mode
    
    if   mode=='raw_to_ana':
        flag_create_ana_file  = True
        flag_save_raw_to_ana  = True
    elif mode=='etp_to_ana':
        flag_create_ana_file  = True
        flag_create_etp_table = True
        flag_save_etp_to_ana  = True
    elif mode=='etp+raw_to_ana':
        flag_create_ana_file  = True
        flag_save_raw_to_ana  = True
        flag_create_etp_table = True
        flag_save_etp_to_ana  = True
    

    print('')
    print('mode:',mode)
    print('flag_create_ana_file \t = ',flag_create_ana_file)
    print('flag_create_etp_table \t = ',flag_create_etp_table)
    print('flag_save_raw_to_ana \t = ',flag_save_raw_to_ana)
    print('flag_save_etp_to_ana \t = ',flag_save_etp_to_ana)    
    print('')
    
    a1tofrange=np.array(a1tofrange)
    a2tofrange=np.array(a2tofrange)
    xrange=np.array(xrange)
    yrange=np.array(yrange)
    

    ###############################################################################
    ###############################################################################
    
    # find maximum iteration file    
    ###############################################################################
    iteration=0
    while True:
        if os.path.isfile( os.path.join(datapath,fnamebase+str(iteration)+fileext ) ):
            iteration+=1
        else:
            break
    maxiteration=iteration-1
    ###############################################################################
    
    # calc start/stopiteration
    ###############################################################################
    if ( startiteration<0 ):
        startiteration=0    
    if (stopiteration<startiteration) or (stopiteration<0):
        stopiteration=maxiteration
    if (maxiteration<stopiteration):
        stopiteration=maxiteration
    ###############################################################################
    
    # init data statistic variables
    ###############################################################################
    Niterations=0
    NsinglesA1=NsinglesA2=C1=C2=itertime=NmcpA1=NmcpA2=0
    
    rawstats={  'itertime':[], 'timestamp':[]
            , 'C1':[], 'C2':[]
            , 'NmcpA1':[], 'NmcpA2':[]
            , 'NsinglesA1':[], 'NsinglesA2':[]
            }  
    etpstats={  'itertime':[], 'timestamp':[]
            , 'C1':[], 'C2':[]
            }  
    ###############################################################################
    
    ###############################################################################
    
    
    ###############################################################################
    # load orig Artof ASC config files
    ASCcfg = ASCcfg_from_text(datapath,cfgfiles=cfgfilelist)    
    ###############################################################################
    if flag_save_etp_to_ana:
        Timage  =  getImageTrafoFromASCcfg(ASCcfg)    
        Tenergy = getEnergyTrafoFromASCcfg(ASCcfg)
    ###############################################################################


    ###############################################################################
    # open tables h5file(s) and create tables and groups
    ###############################################################################
    #
#    outfilebase = outpath+'/'+os.path.split(datapath)[-1]
    outfilebase = os.path.join(outpath,os.path.split(datapath)[-1])
    #
    h5rawfilename = outfilebase+raw_postfix+'.h5'
    # delete raw file if exists
    try:
        os.remove(h5rawfilename)
        # os.copy ... _backup<date>
    except:
        pass
    # save orig. Artof ASC config to h5
    ASCcfg_to_h5(ASCcfg, h5rawfilename, cfgfiles=cfgfilelist, h5path='/')  
    # create dstes with untis
    h5rawfile = tables.open_file(h5rawfilename, mode = "a")
    tabraw={}
    for k in ['coin','acc']:
        tab=tabraw
        tab[k] = h5rawfile.create_table('/xytraw/tables' , k , tdcEvent, createparents=True)
        tab[k].attrs.FIELD_0_UNIT='ps'
        tab[k].attrs.FIELD_1_UNIT='ps'
        tab[k].attrs.FIELD_2_UNIT='ps'
        tab[k].attrs.FIELD_3_UNIT='ps'
        tab[k].attrs.FIELD_4_UNIT='ps'
        tab[k].attrs.FIELD_5_UNIT='ps'
        tab[k].attrs.FIELD_6_UNIT='s'
    ### if etp converting requested
    if flag_create_ana_file:
        h5anafilename = outfilebase + ana_postfix+'.h5'
        # delete file if exists
        try:
            os.remove(h5anafilename)
        # os.copy ... _backup<date>
        except:
            pass
        # save orig. Artof ASC config to h5
        ASCcfg_to_h5(ASCcfg, h5anafilename, cfgfiles=cfgfilelist, h5path='/') 
        h5anafile = tables.open_file(h5anafilename, mode = "a")
    if flag_save_raw_to_ana:
        tabraw2ana={}
        for k in ['coin','acc']:
            tab=tabraw2ana
            tab[k] = h5anafile.create_table('/raw/tables' , k , tdcEvent, createparents=True)
            tab[k].attrs.FIELD_0_UNIT='ps'
            tab[k].attrs.FIELD_1_UNIT='ps'
            tab[k].attrs.FIELD_2_UNIT='ps'
            tab[k].attrs.FIELD_3_UNIT='ps'
            tab[k].attrs.FIELD_4_UNIT='ps'
            tab[k].attrs.FIELD_5_UNIT='ps'
            tab[k].attrs.FIELD_6_UNIT='s'
    if flag_create_etp_table:
        # create dstes with untis
        tabetp={}
        for k in ['coin','acc']:
            tab=tabetp
            tab[k] = h5anafile.create_table('/etp/tables' , k , etpEvent, createparents=True)
            tab[k].attrs.FIELD_0_UNIT='eV'
            tab[k].attrs.FIELD_1_UNIT='rad'
            tab[k].attrs.FIELD_2_UNIT='rad'
            tab[k].attrs.FIELD_3_UNIT='eV'
            tab[k].attrs.FIELD_4_UNIT='rad'
            tab[k].attrs.FIELD_5_UNIT='rad'
            tab[k].attrs.FIELD_6_UNIT='s'
           
    ###############################################################################   


    ############################################################
    ####### the loop over all iterations    
    ############################################################
    iteration=startiteration-1 
    # iterastion is incremented at start of while loop
    # this way, the iteration number at end of loop is the max.iteration
    while True:
        if iteration==stopiteration: break
        iteration=iteration+1
        progressbar(iteration , maxiteration, status='Row {0:3}/{1:3}'.format(iteration,maxiteration) )        
        
        iterationfile=os.path.join(datapath,fnamebase+str(iteration)+fileext)
        if not os.path.isfile( iterationfile ):
         #   print()
         #   print(os.path.join(datapath,fnamebase+str(iteration)+fileext ),' DOES NOT EXIST!, iteration ',iteration)
            print( 'processed iterations '+str(startiteration)+' to '+str(iteration-1)+' \n' )
            print()
            
            break
        if os.path.getsize( iterationfile ) == 0:
            # empty coin file => Move to next
            continue
        
        Niterations=Niterations+1
        
        #print("\r \t Iteration {0:3} / {1:3}".format(iteration,stopiteration), end="\r")
        progressbar(iteration , stopiteration, status='Iteration {0:3}/{1:3}'.format(iteration,stopiteration) )        
                
        
        ###########################################
        # load rawdata from iteration file            
        ###########################################
        dfcoin,dfacc,rawstats = get_rawdataframes_from_iterationfile(
                iterationfile=iterationfile
                ,stats=rawstats
                )
        ###########################################
        
        
        ###########################################
        # save raw data to h5 with tables
        ###########################################
        tab=tabraw
        tab['coin'].append( dfcoin.to_records(index=False) )
        tab['coin'].flush() 
        tab['acc' ].append(  dfacc.to_records(index=False) )
        tab['acc' ].flush() 
        ###########################################


        ###########################################
        # save raw data to analysed file
        ###########################################
        if flag_save_raw_to_ana:
            tab=tabraw2ana
            t1min=1e12*(ASCcfg['a1']['tofrange'][0].min() + ASCcfg['a1']['t0']) - ps_to_expand_TOFrange_for_raw_ana
            t1max=1e12*(ASCcfg['a1']['tofrange'][1].max() + ASCcfg['a1']['t0']) + ps_to_expand_TOFrange_for_raw_ana
            t2min=1e12*(ASCcfg['a2']['tofrange'][0].min() + ASCcfg['a2']['t0']) - ps_to_expand_TOFrange_for_raw_ana
            t2max=1e12*(ASCcfg['a2']['tofrange'][1].max() + ASCcfg['a2']['t0']) + ps_to_expand_TOFrange_for_raw_ana
            
            df=dfcoin
            condition = (t1min<=df.t1) & (df.t1<=t1max) & (t2min<=df.t2) & (df.t2<=t2max)
#            tab['coin'].append(  df[condition].drop(columns=['mask']).to_records(index=False) )
            tab['coin'].append(  df[condition].to_records(index=False) )
            tab['coin'].flush() 
            
            df=dfacc
            condition = (t1min<=df.t1) & (df.t1<=t1max) & (t2min<=df.t2) & (df.t2<=t2max)
#            tab['acc' ].append(  df[condition].drop(columns=['mask']).to_records(index=False) )
            tab['acc' ].append(  df[condition].to_records(index=False) )
            tab['acc' ].flush() 
        ###########################################




        ###########################################
        # calc & save  etp data to analysed file
        ###########################################
        if flag_create_etp_table:
            
            # calc etp from xytraw    
            dfcoin = get_etpdataframe_from_rawdataframe( dfcoin , Timage=Timage , Tenergy=Tenergy, thetamax_degree=thetamax_degree )
            dfacc  = get_etpdataframe_from_rawdataframe( dfacc  , Timage=Timage , Tenergy=Tenergy, thetamax_degree=thetamax_degree )
            # save to h5ana file
            tab=tabetp
            tab['coin'].append( dfcoin.to_records(index=False) )
            tab['coin'].flush() 
            tab['acc' ].append(  dfacc.to_records(index=False) )
            tab['acc' ].flush() 
            ###########################################
        



                   
    ###########################################
    # End of Loop over all iterations
    ###########################################
    try: 
        h5rawfile.close()
    except:
        pass
    try: 
        h5anafile.close()
    except:
        pass
    
    
    
    
    
    ###########################################
    # save stats for this run
    ###########################################
    stats=pd.DataFrame(rawstats)
    with pd.HDFStore(h5rawfilename) as store:
        store.put('xytraw/dfstats', stats) #x format=, append=False, **kwargs)
    
    
    ###########################################
    try: 
        h5rawfile.close()
    except:
        pass
    try: 
        h5anafile.close()
    except:
        pass
    ###########################################
    
    
    print()
    print()
    print('saved data to: \t',h5rawfilename)
    if flag_create_ana_file:
        print('saved data to: \t',h5anafilename)
    print()
   
#    return 0
    return dfcoin,dfacc,stats,ASCcfg
###############################################################################
###############################################################################
###############################################################################
###############################################################################






#"""
