#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 29 14:59:02 2019


Handle ASC config files

usually these are: 
    cfgfilelist = ['acquisition.cfg','detector.txt','elements.txt','timing.txt','extra.txt']

functions in this file:
 
    def ASCcfg_from_text(datapath, cfgfiles='acquisition.cfg'):
    def ASCcfg_from_h5(h5filename, h5path='/'):
    def ASCcfg_to_h5(ASCcfg, h5filename, h5path='/', cfgfiles=['acquisition.cfg']):
    def getImageTrafoFromASCcfg(ASCcfg,N=None):
    def getEnergyTrafoFromASCcfg(ASCcfg,N=None):

    
for internal use:
    def ConvertArtofConfigVector2float(strlist):
        does what name says
        
    def create_cfg_dict_from_ini_and_ASCcfg(ASCcfg):
        evaluation of cfgfile content and fill main ASCcfg dict
    
    def write_txtblock_to_hdf(h5filenname,key,val):
        does what name says


@author: (c) torsten.leitner@gmail.com
"""

import os
import io
import configparser
import warnings
import h5py


import numpy as np






def getImageTrafoFromASCcfg(ASCcfg,N=None):
    import scipy.interpolate
    Timage  = { 'a1':{}, 'a2':{} }
#    Tenergy = { 'a1':{}, 'a2':{} }
    
    for k in ['a1','a2']:
        
        xin= ASCcfg[k]['transformXVector'] / ASCcfg[k]['tickspersecond']
        yin= ASCcfg[k]['transformYVector'] / ASCcfg[k]['tickspersecond']
        
        
#        Timage[k]['Txfunc']=scipy.interpolate.RectBivariateSpline( x, y, ASCcfg[k]['transformXMatrix'] )
#        Timage[k]['Tyfunc']=scipy.interpolate.RectBivariateSpline( x, y, ASCcfg[k]['transformYMatrix'] )
        Timage[k]['Txfunc']=np.vectorize( scipy.interpolate.interp2d( xin, yin, ASCcfg[k]['transformXMatrix'], kind='cubic', copy=True, bounds_error=False, fill_value=np.nan ) , otypes=[np.float64] )
        Timage[k]['Tyfunc']=np.vectorize( scipy.interpolate.interp2d( xin, yin, ASCcfg[k]['transformYMatrix'], kind='cubic', copy=True, bounds_error=False, fill_value=np.nan ) , otypes=[np.float64] )
        
        if N==None: # copy ASC cfg matrices
            Timage[k]['x']  = xin
            Timage[k]['y']  = yin
            Timage[k]['Tx'] = ASCcfg[k]['transformXMatrix'].transpose()
            Timage[k]['Ty'] = ASCcfg[k]['transformYMatrix'].transpose()          
        else:       # innterpolate for N points
            Timage[k]['x']  = np.linspace( np.min(xin), np.max(xin)  , N)
            Timage[k]['y']  = np.linspace( np.min(yin), np.max(yin)  , N)
            Timage[k]['Tx'] = np.zeros([N,N])
            Timage[k]['Ty'] = np.zeros([N,N])
            Timage[k]['Tx'] = Timage[k]['Txfunc'](Timage[k]['x'],Timage[k]['y'])
            Timage[k]['Ty'] = Timage[k]['Tyfunc'](Timage[k]['x'],Timage[k]['y'])

    return Timage



def getEnergyTrafoFromASCcfg(ASCcfg,N=None):
    import scipy.interpolate
    Tenergy  = { 'a1':{}, 'a2':{} }
#    Tenergy = { 'a1':{}, 'a2':{} }
    
    for k in ['a1','a2']:
        
        xin= ASCcfg[k]['tofvector']
        yin= ASCcfg[k]['radiusvector']
        
        Tenergy[k]['t0']=ASCcfg[k]['t0']  

        Tenergy[k]['TEfunc']=np.vectorize( scipy.interpolate.interp2d( xin, yin, ASCcfg[k]['energymatrix'], kind='cubic', copy=True, bounds_error=False, fill_value=np.nan ) , otypes=[np.float64] )
        Tenergy[k]['TTfunc']=np.vectorize( scipy.interpolate.interp2d( xin, yin, ASCcfg[k]['thetamatrix'] , kind='cubic', copy=True, bounds_error=False, fill_value=np.nan ) , otypes=[np.float64] )

        if N==None: # copy ASC cfg matrices
            Tenergy[k]['tof']= xin
            Tenergy[k]['r']  = yin
            Tenergy[k]['TE'] = ASCcfg[k]['energymatrix'].transpose()
            Tenergy[k]['TT'] = ASCcfg[k]['thetamatrix'].transpose()     
        else:       # innterpolate for N points
            Tenergy[k]['tof']= np.linspace( np.min(xin), np.max(xin)  , N)
            Tenergy[k]['r']  = np.linspace( np.min(yin), np.max(yin)  , N)
            Tenergy[k]['TE'] = np.zeros([N,N])
            Tenergy[k]['TT'] = np.zeros([N,N])    
            Tenergy[k]['TE'] = Tenergy[k]['TEfunc']( Tenergy[k]['tof'] , Tenergy[k]['r'] )
            Tenergy[k]['TT'] = Tenergy[k]['TTfunc']( Tenergy[k]['tof'] , Tenergy[k]['r'] )
            
        # data is in picoseconds, hence conversion of the limits to ps
        Tenergy[k]['tmin'] = (Tenergy[k]['tof'].min() + Tenergy[k]['t0']) / 1e-12 # ps
        Tenergy[k]['tmax'] = (Tenergy[k]['tof'].max() + Tenergy[k]['t0']) / 1e-12 # ps
        
        # in meter
        Tenergy[k]['rmin'] = 0
        Tenergy[k]['rmax'] = Tenergy[k]['r'].max()
            
    return Tenergy
    
    
    





### ### ### ### ### ### ### ### ### ### ### ###
# ASC cfg files from text files
### ### ### ### ### ### ### ### ### ### ### ###
def ASCcfg_from_text(datapath, cfgfiles='acquisition.cfg'):
    '''
    # loop over artof 1 & 2 cfg files and fill into
    # ASCcfg= {  'a1':{} , 'a2':{}  } [cfgfile]
    '''
    ArtofCFGpath = { 'a1' : os.path.join( datapath , 'A1cfg')
                   , 'a2' : os.path.join( datapath , 'A2cfg') }
    ASCcfg= {  'a1':{} , 'a2':{}  } 
    for k in ASCcfg: 
        # load all other cfgfiles
        for cfgfile in cfgfiles:
            ### ### ### ### ### ### ### ### ### ### ### ### 
            filename  = os.path.join( ArtofCFGpath[ k ] , cfgfile )
            if not os.path.isfile(filename):
                warnings.warn('in function >> ASCcfg_from_text >> cfgfile: \"'+filename+'\" not found!')
            ### ### ### ### ### ### ### ### ### ### ### ### 
            #cfgparser  = load_ini_file( file )
            cfgparser = configparser.ConfigParser()
            cfgparser.read(filename)    
### ### ### ### ### ### ### ### ### ### ### ### 
            if not cfgparser.has_section('file'): cfgparser.add_section('file')
            cfgparser.set('file', 'name', filename)
            cfgparser.set('file', 'path', ArtofCFGpath[ k ])
            ### ### ### ### ### ### ### ### ### ### ### ### 
            with io.StringIO("") as strIO:   
                cfgparser.write(strIO)
                cfgparser.txt=strIO.getvalue()
            ### ### ### ### ### ### ### ### ### ### ### ###
            ASCcfg[k][cfgfile]=cfgparser
        # end for cfgfile
        
    # end for k (loc/rem)
    #return ASCcfg
    return create_cfg_dict_from_ini_and_ASCcfg(ASCcfg)
### ### ### ### ### ### ### ### ### ### ### ###



### ### ### ### ### ### ### ### ### ### ### ###
# ASC cfg files from h5
### ### ### ### ### ### ### ### ### ### ### ###
def ASCcfg_from_h5(h5filename, h5path='/'):
    '''
    # loop over artof 1 & 2 cfg files and fill into
    # ASCcfg= {  'a1':{} , 'a2':{}  } [cfgfile]
    '''
    ASCcfg= {  'a1':{} , 'a2':{}  } 
    h5file = h5py.File(h5filename, 'r')    
    # load all cfgfiles for each artof
    for k in ['a1','a2']: 
        for cfgfile in h5file[h5path+'/ASCcfg/'+k]:
            key=h5path+'/ASCcfg/'+k+'/'+cfgfile
            cfgstr = str(h5file[key][:])             
            cfgparser = configparser.ConfigParser()
            cfgparser.read_string( cfgstr[2:-2].replace('\\n','\n') )          
            ### ### ### ### ### ### ### ### ### ### ### ### 
            if not cfgparser.has_section('file'): cfgparser.add_section('file')
            cfgparser.set('file', 'name', cfgfile)
            ### ### ### ### ### ### ### ### ### ### ### ### 
            cfgparser.txt=cfgstr
            ### ### ### ### ### ### ### ### ### ### ### ###            
            ASCcfg[k][cfgfile]=cfgparser
        # end for cfgfile        
    # end for k (loc/rem)
    h5file.close()
    return create_cfg_dict_from_ini_and_ASCcfg(ASCcfg)
### ### ### ### ### ### ### ### ### ### ### ###



### ### ### ### ### ### ### ### ### ### ### ###
# ASC cfg files to h5 as text blocks in substructre
### ### ### ### ### ### ### ### ### ### ### ###
def ASCcfg_to_h5(ASCcfg, h5filename, h5path='/', cfgfiles=['acquisition.cfg']):
    '''
    # loop over artof 1 & 2 cfg files and fill into
    # ASCcfg= {  'a1':{} , 'a2':{}  } [cfgfile]
    '''    
    ### ### ### ### ### ### ### ### ### ### ### ###
    for fname in cfgfiles:
        for k in ['a1','a2']:
            key=h5path+'/ASCcfg/'+k+'/'+fname
            val=ASCcfg[k][fname].txt
            write_txtblock_to_hdf(h5filename,key,val)    
###############################################################################
#
###############################################################################
def write_txtblock_to_hdf(h5filenname,key,val):
        import h5py
        with h5py.File(h5filenname, 'a') as h5file:
            str_type = h5py.special_dtype(vlen=str)
            try: 
                del h5file[key]
            except:
                pass
            finally:
                h5file.create_dataset(key, (1,) , dtype=str_type)
                h5file[key][...] = val
###############################################################################





### ### ### ### ### ### ### ### ### ### ### ###
# build cfg dict from cfgparser ini and ASCcfg
### ### ### ### ### ### ### ### ### ### ### ###
def create_cfg_dict_from_ini_and_ASCcfg(ASCcfg):
    '''
cfg = create_cfg_dict_from_ini_and_ASCcfg(ASCcfg)

create 'cfg' (multi-level) dictionary from 'ASCcfg' dictionaries
    including a summary of the more important and calculated parameters.

input:
    ASCcfg : dict with configparser objects for both Artofs and for each cfgfile

output: dict cfg    
    cfg[]               : global parameters for both Artofs
    cfg['a1']            : dict for Artof1 specific params
    cfg['a2']            : dict for Artof2 specific params
    
    cfg['aX'][cfgfile]   : dict from cfgparser object for each ASCcfg file
                          cfgfile=['acq','timing',...]
    cfg['aX'][cfgfile][section][option] => parameter from cfgfile
        -> example:   cfg['a1']['acq']['lensmode']['lensk']

return cfg
    '''

    import numpy as np
    from scipy.optimize import curve_fit
    
        
    
    for k in ['a1','a2']:
        #
        acq=ASCcfg[k]['acquisition.cfg']
       #
#        cfg[k]['artoftype']     = artoftype
#        cfg[k]['distance']      = float(ini[ artoftype ]['sample2detectordistance'] ) # m
        #
        ASCcfg[k]['tickspersecond'] = float(acq['detector']['tdcResolution'])
        ASCcfg[k]['ASCt0']       = float(acq['detector']['t0'])
        ASCcfg[k]['t0']          = ASCcfg[k]['ASCt0'] / ASCcfg[k]['tickspersecond'] # seconds
        #
        ASCcfg[k]['iterations']    = int(acq['general']['lensiterations'])
        ASCcfg[k]['iterationtime'] = 1e-3*float(acq['general']['lensdwelltime']) \
                                * int(acq['general']['lenssteps'])
        ASCcfg[k]['acqtime']       = ASCcfg[k]['iterationtime'] * ASCcfg[k]['iterations']
        ASCcfg[k]['lensmode']      = acq['analyzer']['lensmode']
        ASCcfg[k]['elementset']    = acq['lensmode']['ekinref']
        ASCcfg[k]['lensk']         = float(acq['lensmode']['lensk'])

        ASCcfg[k]['Erange']        = np.array([  float( acq['general']['spectrumbeginenergy'] ) 
                                                ,float( acq['general']['spectrumendenergy'  ] ) ] )
        ASCcfg[k]['Ewin']          = np.abs(ASCcfg[k]['Erange'][1]-ASCcfg[k]['Erange'][0])

        ### energycalibartion from acquisiton.cfg
        Ekref        = float(acq['lensmode']['ekinref'])
        Ekcenter     = float(acq['general'] ['centerenergy'])
        vectorsize   =   int(acq['lensmode']['vectorsize'])
        radiusvector = ConvertArtofConfigVector2float(acq['lensmode']['radiusvector'])
        tofvector    = ConvertArtofConfigVector2float(acq['lensmode']['tofvector'])
        thetamatrix  = ( ConvertArtofConfigVector2float( acq['lensmode']['thetamatrix']  ) ).reshape( (vectorsize,vectorsize) )       
        energymatrix = ( ConvertArtofConfigVector2float( acq['lensmode']['energymatrix'] ) ).reshape( (vectorsize,vectorsize) )       
        enScale     = Ekcenter/Ekref
        enScaleSqrt = np.sqrt(Ekcenter/Ekref)

        # scale reference TOF to energymapping
        #  Ekin( sqrt(a)*tof ) = (1/a) * Ekin(tof) 
        tofvector    = tofvector    / enScaleSqrt
        energymatrix = energymatrix * enScale
        EkOnAxis   = energymatrix[(vectorsize-1)//2,:]  # energy on axis  (note:  "//"gives floor_divide . "%"is the remainder)

        # Ekin(t,t0,A) as str... later: f=eval(str)
        ASCcfg[k]['fit']={}
        fit=ASCcfg[k]['fit']    
        ##################################
        fit['Ekfuncstr']        = "lambda t,   A,t0 : A / (t-t0)**2"
        fit['dEkfuncstr']       = "lambda t,dt,A,t0 : 2*dt*( A / (t-t0)**2 )/(t-t0)"
        fit['inverseEkfuncstr'] = "lambda Ek,A,t0   : (A/Ek)**0.5 + t0"
        #
        Ekfunc       = eval(fit['Ekfuncstr'])
        dEkfunc      = eval(fit['dEkfuncstr'])
        inverseEkfunc= eval(fit['inverseEkfuncstr'])
        ##################################
        ### fit energy conversion function
        # init parameter
        E1,E2,t1,t2 = EkOnAxis[0],EkOnAxis[-1],tofvector[0],tofvector[-1]
        t0init = ( np.sqrt(E2/E1)*t2 - t1 ) / ( np.sqrt(E2/E1)-1 )
        Ainit = E1*np.square(t1-t0init)
        init_vals = [Ainit,t0init]    # for [A, t0]
        best_vals, covar = curve_fit(Ekfunc, tofvector, EkOnAxis, p0=init_vals) 
        fit_A =best_vals[0]
        fit_t0=best_vals[1]    
        ##################################
        fit['A'] =fit_A
        fit['t0']=fit_t0
        fit['Ekfitfuncstr']        = "lambda t,   : "+str(fit_A)+" / (t-"+str(fit_t0)+")**2"
        fit['dEkfitfuncstr']      = "lambda t,dt : 2*dt*( "+str(fit_A)+" / (t-"+str(fit_t0)+")**2 )/(t-"+str(fit_t0)+")"
        fit['inverseEkfitfuncstr']= "lambda Ek   : ("+str(fit_A)+"/Ek)**0.5 + "+str(fit_t0)
        
        inverseEkinFit = eval(fit['inverseEkfitfuncstr'])
        ASCcfg[k]['tofrange']  = np.array( inverseEkinFit(ASCcfg[k]['Erange']) )
        
        ASCcfg[k]['Ekcenter']       = Ekcenter
        ASCcfg[k]['vectorsize']     = vectorsize
        ASCcfg[k]['tofvector']      = tofvector
        ASCcfg[k]['radiusvector']   = radiusvector
        ASCcfg[k]['energymatrix']   = energymatrix
        ASCcfg[k]['thetamatrix']    = thetamatrix
        ASCcfg[k]['EkOnAxis']       = EkOnAxis
        
        # image trafo
        transformVectorSize = int( acq['detector']['transformVectorSize'] )
        ASCcfg[k]['transformXMatrix'] = ( ConvertArtofConfigVector2float(acq['detector']['transformXMatrix']) ).reshape( (transformVectorSize,transformVectorSize) )
        ASCcfg[k]['transformYMatrix'] = ( ConvertArtofConfigVector2float(acq['detector']['transformYMatrix']) ).reshape( (transformVectorSize,transformVectorSize) )
        ASCcfg[k]['transformXVector'] =   ConvertArtofConfigVector2float(acq['detector']['transformXVector'])
        ASCcfg[k]['transformYVector'] =   ConvertArtofConfigVector2float(acq['detector']['transformYVector'])       
        
        
        
        
    return ASCcfg  
### ### ### ### ### ### ### ### ### ### ### ###
      





#
#
# HELPER FUNCS
#
#
######################################################################
# load Artof 1&2 configuration, i.e. acquisition.cfg
def ConvertArtofConfigVector2float(strlist):
    '''
    np.array = ConvertArtofConfigVector2float(config,section,key)
--
config:     configparser object with config
section:    section within config
key:        key of vector to be converted
--
Data vectors in Artof/Scienta config files follow uncommon format and are therefore read as strings.
This function takes such a string and converts it into a float numpy array.
    --
    #'''
    import numpy as np
    #strlist=config[section][key]
    strlist=strlist.replace('[','')
    strlist=strlist.replace(']','')
    strlist=strlist.split(' ') #[1:-1]
    vec=[]
    for i,s in enumerate(strlist):
        vec.append(float(s))
    return np.array(vec)
######################################################################
