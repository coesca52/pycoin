#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 18 18:27:23 2019

@author: coesca
"""


import tables

# from libcoin
from . dfmap_from_table import dfmap_from_table

###############################################################################
###############################################################################
def dfmaps_from_pytables_search( h5name, xrange, yrange, dxy
                               , condition='', xvar='E1', yvar='E2'
                               , dsetkey='etp', histgram_method='weighted'):
    '''
# Examples for Condition string


Defaults:

condition  ='' : the software chooses E1<1e15 which is always TRUE
==> condition  = 'E1<1e15'
    condition += '( theta1 < 22*(3.1415/180) )'
    condition += '( theta2 < 22*(3.1415/180) )'
     


More condition string examples:

condition  =    '( E1 < 337 )'
condition += '&  ( E2 < 470 )'
condition += '&  ( phi1 > 0 )'
condition += '&  ( phi1 < 0.02 )'

condition = '( 799.99 < E1+E2 )&( E1+E2 < 805 )'
condition = '( E1 < 337 ) &  ( E2 < 470 ) &  ( phi1 > 0 ) &  ( phi1 < 0.02 )'

# nothing above 22°
condition += '( theta1 < 22*(3.1415/180) )&( theta2 < 22*(3.1415/180) )'
    
    '''

    # check condition
    if (condition == None) or (condition == ''):
        condition  = '(E1<1e15)'
        condition += '&( theta1 < 22*(3.1415/180) )'
        condition += '&( theta2 < 22*(3.1415/180) )'
    
    ###############################################################################
    #   open h5 file
    ###############################################################################
    with tables.open_file(h5name, mode = "r") as h5file:
        tabcoin=h5file.root['/'+dsetkey+'/tables/coin']
        tabacc =h5file.root['/'+dsetkey+'/tables/acc']
       
    #
        #  Think about introducing chunked histogram processing for longer datasets (i.e. Nrow >> 1e6)
        #
        
        tab= {  'coin': tabcoin.read_where(condition)
              , 'acc' :  tabacc.read_where(condition) }
        key='coin'
        dfcoin  = dfmap_from_table( tab[key][xvar] , tab[key][yvar]  , datarange=[xrange,yrange], dxy=dxy, method=histgram_method)
        key='acc'
        dfacc  = dfmap_from_table( tab[key][xvar] , tab[key][yvar]  , datarange=[xrange,yrange], dxy=dxy, method=histgram_method)
        dftrue=dfcoin-.5*dfacc
        
    return dfcoin,dfacc,dftrue
###############################################################################
###############################################################################
