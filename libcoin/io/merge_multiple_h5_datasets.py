#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 15 15:36:01 2019

@author: (c) torsten.leitner@gmail.com
"""

import os
import h5py
import tables



#################################################################################
def merge_multiple_h5_datasets(inpath, prefix, postfix
        ,outpath=None # if None, outpath=inpath
        ,flag_copy_raw = False, fileext='.h5', outnamelabel = '_ALL_'
        ):
#################################################################################
#################################################################################
    '''
    doc here
    '''
    list_of_files_merged=[]
    if outpath==None: outpath=inpath
    h5name_out = os.path.join(outpath,prefix+outnamelabel+postfix+fileext)
    try:
        os.remove(h5name_out)
        # os.copy ... _backup<date>
    except:
        pass
    # for all entries in directory ...
    i=0
    for entry in os.scandir(inpath):
        # if entry meets filter [prefix,postfix] and is a file ...
        if entry.name.startswith(prefix) and entry.name.endswith(postfix+fileext) and entry.is_file():
            # increment counter
            i+=1
            h5name_in  = entry.path
            list_of_files_merged.append(h5name_in)
            print('reading: '+h5name_in)
            # if first file, then copy ASCcfg and raw&etp tables
            # DO NOT copy any maps, specs or other stuff
            if i==1:  
                with h5py.File(h5name_out, 'w') as h5out:
                    with h5py.File(h5name_in, 'r') as h5in:
                        folder='/ASCcfg'
                        h5in.copy(folder,h5out)
                        folder='/etp'
                        h5out.create_group(folder)
                        try:
                            h5in.copy(folder+'/tables',h5out[folder])
                        except:
                            del h5out[folder] # if copying fails, delete group
                        if flag_copy_raw:
                            folder='/raw'
                            h5out.create_group(folder)
                            try:
                                h5in.copy(folder+'/tables',h5out[folder])
                            except:
                                del h5out[folder] # if copying fails, delete group
            # if second or later file => append    
            else:
                with tables.open_file(h5name_out, mode = "a") as h5out:
                    with tables.open_file(h5name_in, mode = "r") as h5in:
                        folder='etp'
                        tabin=  { 'coin':  h5in.root['/'+folder+'/tables/coin'] 
                                , 'acc' :  h5in.root['/'+folder+'/tables/acc' ] }  
                        tabout= { 'coin': h5out.root['/'+folder+'/tables/coin'] 
                                , 'acc' : h5out.root['/'+folder+'/tables/acc' ] }
                        for k in ['coin','acc']:
                            tabin[k].append_where( tabout[k] ) # appnds tabin to tabout
                            tabout[k].flush
                        if flag_copy_raw:
                            folder='raw'
                            tabin=  { 'coin':  h5in.root['/'+folder+'/tables/coin'] 
                                    , 'acc' :  h5in.root['/'+folder+'/tables/acc' ] }  
                            tabout= { 'coin': h5out.root['/'+folder+'/tables/coin'] 
                                    , 'acc' : h5out.root['/'+folder+'/tables/acc' ] }
                            for k in ['coin','acc']:
                                tabin[k].append_where( tabout[k] ) # appnds tabin to tabout
                                tabout[k].flush
    print()          
    print('merged files to: '+h5name_out)    
    print()          
    return list_of_files_merged
#################################################################################















    
