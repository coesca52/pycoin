#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 20 03:35:31 2019

@author: (c) torsten.leitner@gmail.com
"""

import numpy as np
import pandas as pd

### OWN libs


### define pytables table
from libcoin.io.datastructures import etpdataformat
###

# settings settings.py  => move to coinsettingns.ini
from libcoin.settings import *




###############################################################################
###############################################################################
###############################################################################




###############################################################################
def get_etpdataframe_from_rawdataframe( df , Timage , Tenergy, thetamax_degree=20): 
    
    
    
    thetamax = np.pi*(thetamax_degree/180)    

    cond_t  =    (     (df['t1'] >= Tenergy['a1']['tmin']) 
                     & (df['t1'] <= Tenergy['a1']['tmax'])
                     & (df['t2'] >= Tenergy['a2']['tmin']) 
                     & (df['t2'] <= Tenergy['a2']['tmax'])   )
#                
#                t1    = [ 1e-12*row['t1']-Tenergy['a1']['t0'] for row in table.where(cond_t, start=i1, stop=i2, step=1) ]
    t1    = 1e-12*df[cond_t]['t1'] - Tenergy['a1']['t0']
    x1raw = 1e-12*df[cond_t]['x1']
    y1raw = 1e-12*df[cond_t]['y1']
    t2    = 1e-12*df[cond_t]['t2'] - Tenergy['a2']['t0']
    x2raw = 1e-12*df[cond_t]['x2']
    y2raw = 1e-12*df[cond_t]['y2']
    trig  = 1e-12*df[cond_t]['trig']

    x1 = Timage['a1']['Txfunc'](x1raw,y1raw)
    y1 = Timage['a1']['Tyfunc'](x1raw,y1raw)
    x2 = Timage['a2']['Txfunc'](x2raw,y2raw)
    y2 = Timage['a2']['Tyfunc'](x2raw,y2raw)
    
    r1 = np.sqrt( np.square(x1) + np.square(y1) )
    r2 = np.sqrt( np.square(x2) + np.square(y2) ) 
                
    # filter for rmin, rmax
    cond_r  =    (     (r1 >= Tenergy['a1']['rmin']) 
                     & (r1 <= Tenergy['a1']['rmax'])
                     & (r2 >= Tenergy['a2']['rmin']) 
                     & (r2 <= Tenergy['a2']['rmax'])   )    
        
    # init output dataframe          
    Nout  = np.sum(cond_r)
    dfout = pd.DataFrame( np.zeros( Nout ,dtype=etpdataformat ) )
    # calc etp output and fill dfout
    dfout['E1']     = Tenergy['a1']['TEfunc'](t1[cond_r] ,r1[cond_r])
    dfout['theta1'] = Tenergy['a1']['TTfunc'](t1[cond_r] ,r1[cond_r])
    dfout['phi1']   = np.arctan2( x1[cond_r], y1[cond_r] ) #+ rotphi1
    dfout['E2']     = Tenergy['a2']['TEfunc'](t2[cond_r] ,r2[cond_r])
    dfout['theta2'] = Tenergy['a2']['TTfunc'](t2[cond_r] ,r2[cond_r])
    dfout['phi2']   = np.arctan2( x2[cond_r], y2[cond_r] ) #+ rotphi2
    dfout['trig']   = np.array(trig,dtype='f8')[cond_r]
    
    
    return dfout[ (dfout.theta1<=thetamax) & (dfout.theta2<=thetamax) ]
###############################################################################



