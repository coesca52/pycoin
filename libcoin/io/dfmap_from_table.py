#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul  3 16:00:06 2019

@author: (c) torsten.leitner@gmail.com
"""

# -*- coding: utf-8 -*-
"""
Created on Thu Jun 27 22:19:36 2019

@author: local_admin
"""
import numpy as np
import pandas as pd


#################################################################################
#################################################################################



#################################################################################
def dfmap_from_table(xdata,ydata, datarange=None, dxy=None, Nbins=None, method='np.histogram2d'):
#################################################################################
    '''
    Nbins flag overrides dxy flag
    method:     'np.histogram2d'  or  'weighted'
    #'''
    if datarange==None:
        datarange=[ [min(xdata),max(xdata)] , [min(ydata),max(ydata)] ]
    if Nbins is not None:
        Nxbins=Nybins=Nbins
    elif dxy==None:
        Nxbins=101
        Nybins=101
    else:
        try:
            dx=dxy[0]
            dy=dxy[1]
        except:
            dx=dxy
            dy=dxy
        Nxbins = np.int( np.abs( np.diff(datarange[0]) ) / dx )
        Nybins = np.int( np.abs( np.diff(datarange[1]) ) / dy )
        
        
    # axis
    x,dx=np.linspace(datarange[0][0],datarange[0][1],Nxbins, endpoint=True, retstep=True)
    y,dy=np.linspace(datarange[1][0],datarange[1][1],Nybins, endpoint=True, retstep=True)

      
    ### 
    if method=='np.histogram2d':
        
        # calc the histogram
        h = np.histogram2d(xdata, ydata, bins=[Nxbins,Nybins], range=datarange, normed=None, weights=None, density=None)  
        # 2D histograms as DataFrames
        df = pd.DataFrame(h[0])
        df.set_axis(x, axis='index'  , inplace=True)
        df.set_axis(y, axis='columns', inplace=True)
        
        
    elif method=='weighted':        
        #'''

        # condition for data
        condition=np.where( (min(x)<=xdata)&(xdata<max(x)) & (min(y)<=ydata)&(ydata<max(y)) )

    #    scale = data / delta
        xd=xdata[condition].copy()-min(x)
        yd=ydata[condition].copy()-min(y)
        scx= (xd) / dx
        scy= (yd) / dy

    #    index = floor(data)
        ix = np.floor(scx).astype(int) 
        iy = np.floor(scy).astype(int) 

    #    ratio = counts(ix+1) / counts(ix)
        rx = 1 - scx - ix
        ry = 1 - scy - iy
        # rxy  - distance of gridpoints to hitcoordinate
        # gridpoints: i,j=00, i+1,j=10, i+1,j+1=11, i+1,j+1=01, 
        r00 = np.sqrt(     rx**2 + ry**2 )
        r10 = np.sqrt( (1-rx)**2 + ry**2 )
        r11 = np.sqrt( (1-rx)**2 + (1-ry)**2 )
        r01 = np.sqrt(     rx**2 + (1-ry)**2 )
        # normalise rxy to be 1 in sum
        rsum=r00+r10+r11+r01
        r00=r00/rsum
        r10=r10/rsum
        r11=r11/rsum
        r01=r01/rsum
        
        # initialise np.array    
        h=np.zeros((Nxbins,Nybins))
        
        # fill the histogram matrix
        for k in range(len(ix)):
            i,j = ix[k],iy[k]
            h[i,j] +=  r00[k]
            try:  h[i+1,j]   +=  r10[k] 
            except:  pass
            try:  h[i+1,j+1] +=  r11[k] 
            except:  pass
            try:  h[i,j+1]   +=  r01[k] 
            except:  pass
       #'''
        
        # 2D histograms as DataFrames
        df = pd.DataFrame(h)
        df.set_axis(x, axis='index'  , inplace=True)
        df.set_axis(y, axis='columns', inplace=True)
        
        
        # throw away outer most element of map (instabilities for edge of hiostorgram)
        
        
        


    else:
        print('unknown method given')
        return 1
        
    return df.iloc[1:-1, 1:-1]
#################################################################################
    


