# -*- coding: utf-8 -*-
# (c) 2019 torsten.leitner@gmail.com
#

'''
└── pycoin/libcoin/
     │  
     ├── __init__.py
     ├── settings.py
     │
     ├── analyse/
     │   ├── __init__.py
     │   ├── binning.py
     │   ├── shirley.py
     │   └── smooth.py
     │
     ├── io/
     │   ├── __init__.py
     │   ├── ASCcfg.py
     │   ├── datastructures.py
     │   ├── dfmap_from_table.py
     │   ├── dfmaps_from_pytables_search.py
     │   ├── get_etpdataframe_from_rawdataframe.py
     │   ├── get_rawdataframes_from_iterationfile.py
     │   ├── h5helper.py
     │   ├── merge_multiple_h5_datasets.py
     │   └── rawcoindataset_to_h5.py
     │
     ├── plot/
     │   ├── __init__.py
     │   ├── plot_all3maps_frames_in_true_only.py
     │   ├── plot_allinone_collage.py
     │   ├── plot_helpers.py
     │   ├── plot_map_with_spectra.py
     │   └── plot_pcolor_map.py
     │
     └── tools/
         ├── __init__.py
         ├── progressbar.py
         └── tools.py

#'''

#import io
