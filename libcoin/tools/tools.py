# -*- coding: utf-8 -*-
#
# (c) 2019 torsten.leitner@gmail.com
#
'''
##########################################
various helpers for pycoin:
##########################################
functions:

sigma2fwhm(sigma)
fwhm2sigma(fwhm)
    - Convert sigma to fwhm and vice versa for Gaussian pulses

##########################################
'''

import numpy as np


##########################################
def sigma2fwhm(sigma):
    return sigma * np.sqrt(8 * np.log(2))
##########################################
def fwhm2sigma(fwhm):
    return fwhm / np.sqrt(8 * np.log(2))
##########################################

