#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 15 15:36:01 2019

@author: (c) torsten.leitner@gmail.com
"""

# own libs from pycoin folder one level above
pycoinpath='../'
import sys
sys.path.append(pycoinpath) # add libcoin path here (if not installed as module)

import libcoin
from libcoin.io.merge_multiple_h5_datasets import merge_multiple_h5_datasets


# input data path
inpath='../testdata'

# define dataset(s)
# prefix and postfix for filename: this enables to denote multiple files for processing
# all files which start with prefix and end with postfix will be merged, no matter how they are named inbetween
prefix='Ag3dMNN_coin_data'
postfix='analysed'
#postfix='xytraw'


merge_multiple_h5_datasets(   inpath,   prefix,   postfix
#        ,outpath=None # if None, outpath=inpath
#        ,flag_copy_raw = False, fileext='.h5', outnamelabel = '_ALL_'
        )

