
### ### ### ### ### ### ### ### ### ### ### ###
from scipy.special import wofz
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt 
import pandas as pd
### ### ### ### ### ### ### ### ### ### ### ###
def VoigtProfile(x, area, x0, alpha, gamma):
    """
    Return the Voigt line shape at x with Lorentzian component HWHM gamma
    and Gaussian component HWHM alpha.
    The area of the profile is scaled to match the parameter 'area'.
    uses: from scipy.special import wofz
    """
    #from scipy.special import wofz
    gamma=0
    sigma = alpha / np.sqrt(2 * np.log(2))         
    out   = np.real(wofz(( (x-x0) + 1j*gamma)/sigma/np.sqrt(2))) / sigma /np.sqrt(2*np.pi)
    out  *= area
    return out
### ### ### ### ### ### ### ### ### ### ### ###



### ### ### ### ### ### ### ### ### ### ### ###
def funcShirleyBG(y,BGscale,Offset):
    out  = y[::-1].cumsum()[::-1]  #reverse cumsum 
    out -= out.min()
    out /= out.max()
    return BGscale*out + Offset
### ### ### ### ### ### ### ### ### ### ### ###
def guessShirleyBG(y,edgesize=5):
    left = y[0:edgesize-1].mean()
    right= y[-edgesize:-1].mean()
    #
    Offset = min(left,right)
    BGscale= max(left,right) - Offset
    return BGscale,Offset
### ### ### ### ### ### ### ### ### ### ### ###









figlist=[]
axlist=[]
for k in datdict:
    print(k)
    
    dat=datdict[k]
    ### ### ### ### ### ### ### ### ### ### ### ###
    xdata = np.array(dat.index)
    ydata = np.array(dat.values)
    ydata_err = np.sqrt(abs(ydata))
    # initial guess params
    peak1_x0 = peak1_pos
    peak2_x0 = peak2_pos
    peak3_x0 = peak3_pos
    peak4_x0 = peak4_pos
    area     = .5*ydata.sum()*(xdata[1]-xdata[0])
    alpha    = .2
    gamma    = .2
    BGscale,Offset = guessShirleyBG(ydata) #; print(BGscale,Offset)
    ### ### ### ### ### ### ### ### ### ### ### ###

    

    ### ### ### ### ### ### ### ### ### ### ### ###
    # parts of fit model
    peak1func   = VoigtProfile      #(x, x0, amp, alpha, gamma)
    peak1params = { 'area':area , 'x0': peak1_x0 , 'alpha':alpha , 'gamma':gamma} ; 
    peak1bounds = { 'area':[0,np.inf] , 'x0': [peak1_x0-dpeak,peak1_x0+dpeak] , 'alpha':[0,np.inf] , 'gamma':[0,np.inf]}
    peak1params = {'peak1_'+k: v for k, v in peak1params.items()}
    peak1bounds = {'peak1_'+k: v for k, v in peak1bounds.items()}
    #
    peak2func   = VoigtProfile      #(x, x0, amp, alpha, gamma)
    peak2params = { 'area':area , 'x0': peak2_x0 , 'alpha':alpha , 'gamma':gamma}
    peak2bounds = { 'area':[0,np.inf] , 'x0': [peak2_x0-dpeak,peak2_x0+dpeak] ,'alpha':[0,np.inf] , 'gamma':[0,np.inf]}
    peak2params = {'peak2_'+k: v for k, v in peak2params.items()}
    peak2bounds = {'peak2_'+k: v for k, v in peak2bounds.items()}
    #
    peak3func   = VoigtProfile      #(x, x0, amp, alpha, gamma)
    peak3params = { 'area':area , 'x0': peak3_x0 , 'alpha':alpha , 'gamma':gamma}
    peak3bounds = { 'area':[0,np.inf] , 'x0': [peak3_x0-dpeak,peak3_x0+dpeak] ,'alpha':[0,np.inf] , 'gamma':[0,np.inf]}
    peak3params = {'peak3_'+k: v for k, v in peak3params.items()}
    peak3bounds = {'peak3_'+k: v for k, v in peak3bounds.items()}
    #
    #
    peak4func   = VoigtProfile      #(x, x0, amp, alpha, gamma)
    peak4params = { 'area':area , 'x0': peak4_x0 , 'alpha':alpha , 'gamma':gamma}
    peak4bounds = { 'area':[0,np.inf] , 'x0': [peak4_x0-dpeak,peak4_x0+dpeak] ,'alpha':[0,np.inf] , 'gamma':[0,np.inf]}
    peak4params = {'peak4_'+k: v for k, v in peak4params.items()}
    peak4bounds = {'peak4_'+k: v for k, v in peak4bounds.items()}
    #
    # BG
    BGfunc   = funcShirleyBG        #(y,BGscale,Offset)
    BGparams = { 'BGscale':BGscale    , 'Offset':Offset }
    BGbounds = { 'BGscale':[0,np.inf] , 'Offset':[0,np.inf] }
    ### ### ### ### ### ### ### ### ### ### ### ###
    init      ={**peak1params, **peak2params, **peak3params, **peak4params, **BGparams}
    boundaries={**peak1bounds, **peak2bounds, **peak3bounds, **peak4bounds, **BGbounds}
    fitparams = pd.DataFrame.from_dict(init, orient='index', dtype=None, columns=['init'])
    fitparams['lbound']=-np.inf
    fitparams['ubound']= np.inf
    for kk in boundaries:
        a=boundaries[kk][0]
        b=boundaries[kk][1]
        #print(kk,a,b)
        fitparams.loc[kk,['lbound']]=a
        fitparams.loc[kk,['ubound']]=b
    ### ### ### ### ### ### ### ### ### ### ### ###
    
    ### ### ### ### ### ### ### ### ### ### ### ###
    def fitfunc(x,*args):
        """
        fit func with 3 Voigt profiles and constant background
        """
        y = peak1func(x, *args[0:4])
        y+= peak2func(x, *args[4:8])
        y+= peak3func(x, *args[8:12])
        y+= peak4func(x, *args[12:16])
        y+= BGfunc(y   , *args[-2::])
        return y
    ### ### ### ### ### ### ### ### ### ### ### ###

    # put together p0
    p0 = []
    for v in init.values():     
        p0.append(v)
    # put together bounds
    bounds = ( [] , [] )
    for v in boundaries.values():
        bounds[0].append(v[0])
        bounds[1].append(v[1])
    
    #print(bounds)
    
    yinit=fitfunc(xdata,*p0)
    pfit=None
    pfit_covariance=None
    #pfit, pfit_covariance = curve_fit(fitfunc, xdata, ydata, p0=p0, bounds=bounds)
    pfit, pfit_covariance = curve_fit(fitfunc, xdata, ydata, p0=p0
                                     ,bounds=bounds
                                     ,sigma=ydata_err
                                     ,absolute_sigma=True)
    perr = np.sqrt(np.diag(pfit_covariance))
    fitparams['value']=pfit
    fitparams['abs.err.']=perr
    fitparams['rel.err.']=np.abs(perr/pfit)
    
    yfit = fitfunc(xdata,*pfit)
    yfitpeak1 = peak1func(xdata, *pfit[0:4])
    yfitpeak2 = peak2func(xdata, *pfit[4:8])
    yfitpeak3 = peak3func(xdata, *pfit[8:12])
    yfitpeak4 = peak4func(xdata, *pfit[12:16])
    yfitBG    = BGfunc(yfitpeak1+yfitpeak2+yfitpeak3+yfitpeak4 , *pfit[-2::])
    
    AreaRatio = pfit[4]/pfit[0]
    AreaRatio_err = np.sqrt((perr[0]/pfit[0])**2+(perr[4]/pfit[4])**2) * AreaRatio
    datdict[k].fitresult=fitparams
    datdict[k].fitresult.AreaRatio = AreaRatio
    datdict[k].fitresult.AreaRatio_err = AreaRatio_err
    
#    print("Ratio of Areas Peak2/Peak1 = {0:.3f} +/- {1:.3f}".format(AreaRatio,AreaRatio_err))
#   print("Peak Areas    : {0:.3f}    & {1:.3f}   ".format(pfit[0],pfit[4]))
#    print("Peak sigmas   : {0:.3f}    & {1:.3f}   ".format(pfit[2],pfit[6]))
#    print("Peak gammas   : {0:.3f}    & {1:.3f}   ".format(pfit[3],pfit[7]))
#    print("Peak Positions: {0:.3f} eV & {1:.3f} eV".format(pfit[1],pfit[5]))
     
    print("Peak Areas    : {0:.3f}    & {1:.3f}    & {2:.3f}   ".format(pfit[0],pfit[4],pfit[8],pfit[12]))
    print("Peak sigmas   : {0:.3f}    & {1:.3f}    & {2:.3f}   ".format(pfit[2],pfit[6],pfit[10]))
    print("Peak gammas   : {0:.3f}    & {1:.3f}    & {2:.3f}   ".format(pfit[3],pfit[7],pfit[11]))
    print("Peak Positions: {0:.3f} eV & {1:.3f} eV & {2:.3f} eV & {2:.3f} eV".format(pfit[1],pfit[5],pfit[9],pfit[13]))
     
    
    
    fig, axes = plt.subplots(1, 1, figsize=(15, 10))
    
    ax=axes#[0]
    #ax.plot(xdata,yinit,':',label='initial guess')
    ax.fill_between(xdata,ydata-.5*ydata_err, y2=ydata+.5*ydata_err,alpha=.15,color='tab:blue')
    #ax.errorbar(xdata, ydata, yerr=ydata_err, fmt='.',color='black')
    ax.plot(xdata,yfitBG,':',label='background')
    ax.plot(xdata,yfit,'-r',label='best fit',linewidth=2)
    ax.plot(xdata,ydata,'.k',label='data')
    ax.plot(xdata,yfit-ydata,'-.',label='residual')
    ax.plot(xdata,yfitpeak1+yfitBG,'-',label='peak 1',color='tab:green')
    ax.plot(xdata,yfitpeak2+yfitBG,'-',label='peak 2',color='tab:purple')
    ax.plot(xdata,yfitpeak3+yfitBG,'-',label='peak 3',color='tab:blue')
    ax.plot(xdata,yfitpeak4+yfitBG,'-',label='peak 3',color='tab:orange')
    ax.plot(xdata,yfitpeak1,'--',label='peak 1',color='tab:green')
    ax.plot(xdata,yfitpeak2,'--',label='peak 2',color='tab:purple')
    ax.plot(xdata,yfitpeak3,'--',label='peak 3',color='tab:blue')
    ax.plot(xdata,yfitpeak4,'--',label='peak 3',color='tab:orange')
    #ax.plot(xdata,ydata,'.')
    ax.set_title('Ag 3d '+k)
    ax.set_xlabel('Eb (eV)')
    ax.set_ylabel('counts')
    ax.grid(True, color='tab:gray', linestyle='--')
    ax.legend()
    
    
    if flag_savefig:
        picname = os.path.join(picpath,picprefix+'_'+'Ag3d_'+k+'.pdf')
        plt.savefig(picname,orientation='portrait')
    
    
    plt.show()
    
#endfor
print()



