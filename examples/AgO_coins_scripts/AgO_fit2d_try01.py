
xlabel='E Auger (eV)'
ylabel='E PES (eV)'

# Flatten the guess parameter list.
p0 = [p for prms in guess_prms for p in prms]
# ravel the data
xydata = np.vstack((X.ravel(), Y.ravel())) ; zdata  = Z.ravel()
Zguess = np.zeros(Z.shape)
for i in range(len(p0)//5):  Zguess += gaussian(X, Y, *p0[i*5:i*5+5])

    
# Plot the test data as a 2D image and the fit as overlaid contours.
if 0:
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.imshow(Z, origin='bottom', cmap=cmap,#'plasma',
              extent=(x.min(), x.max(), y.min(), y.max()))
    #ax.contour(X, Y, fit, colors='w')
    ax.contour(X, Y, Zguess, colors='w')

    ax.set_xlim([500,508])
    ax.set_ylim([311,316])
    plt.show()

# Do the fit, using our custom _gaussian function which understands our
# flattened (ravelled) ordering of the data points.
#'''
# x0, y0, xalpha, yalpha, A
# put together bounds
dx0, dy0 = 100, 100
'''
bounds=( [ p0[0]-dx0, p0[1]-dy0, 0, 0, 0
          ,p0[6]-dx0, p0[7]-dy0, 0, 0, 0 ]
        ,[ p0[0]+dx0, p0[1]+dy0, +np.inf, +np.inf, +np.inf
          ,p0[6]+dx0, p0[7]+dy0, +np.inf, +np.inf, +np.inf ]  
       )
#'''

if 1:
    popt, pcov = curve_fit(_gaussian, xydata, Z.ravel(), p0
            #              ,bounds=bounds
                          )

    #popt[2] = 15 ;popt[3] = 3 ;popt[7] = 2 ;popt[8] = 1 

    Zfit = np.zeros(Z.shape)
    for i in range(len(popt)//5):
        Zfit += gaussian(X, Y, *popt[i*5:i*5+5])

    rms = np.sqrt(np.mean((Z - Zfit)**2))
#'''



#ax,cb=fig1.get_axes()
#fig1
if 0:
    # Plot the test data as a 2D image and the fit as overlaid contours.
    fig = plt.figure()
    ax = fig.subplots(1,1)
    ax.imshow(Z, origin='bottom', cmap=cmap,#'plasma',
              extent=(x.min(), x.max(), y.min(), y.max()))
    ax.contour(X, Y, Zfit, colors='w')
    ax.contour(X, Y, Zguess, colors='tab:blue')

    ax.set_xlim([497,512])
    ax.set_ylim([310,317])
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    plt.show()


figsize = (10,5)

# Plot the 3D figure of the fitted function and the residuals.
if 0:
    fig = plt.figure(figsize=figsize)
    offset=-20
    ax = fig.gca(projection='3d')
    ax.plot_surface(X, Y, Zfit, cmap='plasma')
    cset = ax.contourf(X, Y, Z-Zfit, zdir='z', offset=offset, cmap='plasma')
    ax.set_zlim(offset,np.max(Zfit))
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    plt.show()

if 1:
    fig = plt.figure(figsize=figsize)
    Zplot=Z-Zguess
    ax = fig.gca(projection='3d') ; ax.set_title('Z-Zguess')
    ax.plot_surface(X, Y, Zplot, cmap=cmap,alpha=0.5)
    ax.set_zlim(np.min(Zplot),np.max(Zplot))
    ax.view_init(*viewinit)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    plt.show()
        
if 1:
    fig = plt.figure(figsize=figsize)
    Zplot=Zguess
    ax = fig.gca(projection='3d') ; ax.set_title('Zguess')
    ax.plot_surface(X, Y, Zplot, cmap=cmap,alpha=0.5)
    ax.set_zlim(np.min(Zplot),np.max(Zplot))
    ax.view_init(*viewinit)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    plt.show()

if 1:
    fig = plt.figure(figsize=figsize)
    Zplot=Z
    ax = fig.gca(projection='3d') ; ax.set_title('Z')
    ax.plot_surface(X, Y, Zplot, cmap=cmap,alpha=0.5)
    ax.set_zlim(np.min(Zplot),np.max(Zplot))
    ax.view_init(*viewinit)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    plt.show()

if 1:
    fig = plt.figure(figsize=figsize)
    Zplot=Zfit
    ax = fig.gca(projection='3d') ; ax.set_title('Zfit')
    ax.plot_surface(X, Y, Zplot, cmap=cmap,alpha=0.5)
    ax.set_zlim(np.min(Zplot),np.max(Zplot))
    ax.view_init(*viewinit)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    plt.show()

if 1:
    fig = plt.figure(figsize=figsize)
    Zplot=Z-Zfit
    ax = fig.gca(projection='3d') ; ax.set_title('Z-Zfit')
    ax.plot_surface(X, Y, Zplot, cmap=cmap,alpha=0.5)
    ax.set_zlim(np.min(Zplot),np.max(Zplot))
    ax.view_init(*viewinit)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    plt.show()




    
print('Guessed parameters:')
print(p0)
if 1:
    print('Fitted parameters:')
    #print(popt)
    for i in range(len(popt)//5):
        #print(popt[i*5:i*5+5])
        print("{:6.2f}, {:6.2f}, {:6.2f}, {:6.2f}, {:6.2f}".format(*popt[i*5:i*5+5]))
    print('RMS residual =', rms)
    

 