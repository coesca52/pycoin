
# define Regions-of-Interest (ROIs) as frames in the map
ROIs=[]
a1maxrange=[df.index.min(),df.index.max()]
a2maxrange=[df.columns.min(),df.columns.max()]

EcutPES=313.6
ROIs.append( {'a1range':a1maxrange,  'a2range':[308.0,EcutPES],   'txt':'PES Eklow'       ,'linecolor':'tab:blue','marker':'x'    } )
ROIs.append( {'a1range':a1maxrange,  'a2range':[EcutPES+.01,320.0],   'txt':'PES Ekhigh'       ,'linecolor':'tab:orange','marker':'+'  } )

EcutAuger=504.5
ROIs.append( {'a1range':[485.0,EcutAuger],  'a2range':a2maxrange,   'txt':'Auger Eklow'       ,'linecolor':'tab:green','marker':'x'    } )
ROIs.append( {'a1range':[EcutAuger+.01,515.5],  'a2range':a2maxrange,   'txt':'Auger Ekhigh'       ,'linecolor':'tab:purple','marker':'+'  } )


# calc stuff for each ROIs
for roi in ROIs:
    tmpdf=df.loc[(roi['a1range'][0]<=df.index)  &(df.index  <=roi['a1range'][1])
                ,(roi['a2range'][0]<=df.columns)&(df.columns<=roi['a2range'][1])]
    roi['a1']=(tmpdf.sum(axis=1))
    roi['a2']=(tmpdf.sum(axis=0))
### ### ### ### ### ###

# Singles from Acc counts
singles  = { 'a1':(dfacc.sum(axis=1))
            ,'a2':(dfacc.sum(axis=0)) }
# All True Coins
fulltrue = { 'a1':(dftrue.sum(axis=1) )
            ,'a2':(dftrue.sum(axis=0) )   }