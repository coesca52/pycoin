#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Dec 14 00:30:56 2019

AgO_bin_avg_shirley.py

@author: (c) torsten.leitner@gmail.com
"""


# plot  map    
if flag_plotmap:          
    ax3,fig3 = plot_pcolor_map(df,par,cmap=cc) #;print(df.sum().sum())

    # plot spectra for binned map
if flag_plot_specs:
    fig, axes = plt.subplots(1, 2, figsize=(15, 5))
    istep=25
    maxmax=df.max().max()
    # Auger
    ax=axes[0];ax.set_title('Auger')
    mm=0
    for i in range(df.shape[1]):
        line=df.iloc[:,i*istep:(i+1)*istep].mean(axis=1)
        if np.max(line)>mm: mm=np.max(line)
        ax.plot(line)
    ax.plot( maxmax*df.sum(axis=1)/df.sum(axis=1).max() ,':k',label='sum, scaled')
    ax.plot( footprintAuger ,'.b',label='footprint / side view')
    ax.legend()
    # PEs
    ax=axes[1];ax.set_title('PES')
    for i in range(df.shape[0]):
        line=df.iloc[i*istep:(i+1)*istep,:].mean(axis=0)
        if np.max(line)>mm: mm=np.max(line)
        ax.plot(line)
    ax.plot( maxmax*df.sum(axis=0)/df.sum(axis=0).max() ,':k',label='sum, scaled')
    ax.plot(footprintPES ,'.b',label='footprint / side view')
    ax.legend()



















