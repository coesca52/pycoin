# Ag3dMNN_FitAuger_fitdef.py 

### ### ### ### ### ### ### ### ### ### ### ###
from scipy.special import wofz
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt 
import pandas as pd
### ### ### ### ### ### ### ### ### ### ### ###


### ### ### ### ### ### ### ### ### ### ### ###
def Gaussian(x, area, x0, alpha):
    """ Return Gaussian line shape at x with HWHM alpha """
    return area * np.sqrt(np.log(2) / np.pi) / alpha\
                             * np.exp(-( (x-x0) / alpha)**2 * np.log(2))
### ### ### ### ### ### ### ### ### ### ### ###
def Lorentzian(x, gamma):
    """ Return Lorentzian line shape at x with HWHM gamma """
    return area * gamma / np.pi / ( (x-x0)**2 + gamma**2)
### ### ### ### ### ### ### ### ### ### ### ###
def VoigtProfile(x, area, x0, alpha, gamma):
    """
    Return the Voigt line shape at x with Lorentzian component HWHM gamma
    and Gaussian component HWHM alpha.
    The area of the profile is scaled to match the parameter 'area'.
    uses: from scipy.special import wofz
    """
    #from scipy.special import wofz
    sigma = alpha / np.sqrt(2 * np.log(2))         
    out   = np.real(wofz(( (x-x0) + 1j*gamma)/sigma/np.sqrt(2))) / sigma /np.sqrt(2*np.pi)
    out  *= area
    return out
### ### ### ### ### ### ### ### ### ### ### ###



### ### ### ### ### ### ### ### ### ### ### ###
def funcShirleyBG(y,BGscale,Offset):
    out  = y[::-1].cumsum()[::-1]  #reverse cumsum 
    out -= out.min()
    out /= out.max()
    return BGscale*out + Offset
### ### ### ### ### ### ### ### ### ### ### ###
def guessShirleyBG(y,edgesize=5):
    left = y[0:edgesize-1].mean()
    right= y[-edgesize:-1].mean()
    Offset = min(left,right)
    BGscale= max(left,right) - Offset
    return BGscale,Offset
### ### ### ### ### ### ### ### ### ### ### ###

### ### ### ### ### ### ### ### ### ### ### ###
def nPeaks_and_BG(x,*args):
    """
    fit func with N Voigt profiles and shirley background
    """
    y=0*x.copy()
    arg0,arg1=0,0
    for peak in peaks:
        arg0=arg1
        arg1=arg0+peak['nargs']-1
        y += peak['func'](x,*args[arg0:arg1])
    y+= BGfunc(y   , *args[-(BGnargs-1)::])
    return y
### ### ### ### ### ### ### ### ### ### ### ###


