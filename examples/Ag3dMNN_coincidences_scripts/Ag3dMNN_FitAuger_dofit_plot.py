# Ag3dMNN_FitAuger_dofit_plot.py


figlist=[]
axlist=[]
for k in datdict:
    print(k)
    
    dat=datdict[k]
    ### ### ### ### ### ### ### ### ### ### ### ###
    xdata = np.array(dat['a1'].index)
    ydata = np.array(dat['a1'].values)
    ydata_err = np.sqrt(abs(ydata))
    #plt.plot(xdata,ydata);plt.show()
    ### ### ### ### ### ### ### ### ### ### ### ###
    
    BGscale,Offset = guessShirleyBG(ydata) 
    BGparams = { 'BGscale':BGscale    , 'Offset':Offset }

    ### ### ### ### ### ### ### ### ### ### ### ###
    init={}
    boundaries={}
    for peak in peaks:
        init       = {**init, **{peak['label']+kk: v for kk, v in peak['init'].items()}}
        boundaries = {**boundaries, **{peak['label']+kk: v for kk, v in peak['bounds'].items()}}
    init       = {**init, **BGparams}
    boundaries = {**boundaries, **BGbounds}
    fitfunc=nPeaks_and_BG
    ### ### ### ### ### ### ### ### ### ### ### ###
    
    ### ### ### ### ### ### ### ### ### ### ### ###
#    fitparams = pd.DataFrame.from_dict(init, orient='index', dtype=None, columns=['init'])
#    fitparams['lbound']=-np.inf
#    fitparams['ubound']= np.inf
#    for kk in boundaries:
#        fitparams.loc[kk,['lbound']]=boundaries[kk][0]
#        fitparams.loc[kk,['ubound']]=boundaries[kk][1]
    ### ### ### ### ### ### ### ### ### ### ### ###


    ### ### ### ### ### ### ### ### ### ### ### ###
    # put together p0
    p0 = []
    for v in init.values():     
        p0.append(v)
    # put together bounds
    bounds = ( [] , [] )
    for v in boundaries.values():
        bounds[0].append(v[0])
        bounds[1].append(v[1])
    ### ### ### ### ### ### ### ### ### ### ### ###
    
    
    ### ### ### ### ### ### ### ### ### ### ### ###
    yinit=fitfunc(xdata,*p0)
    pfit=None
    pfit_covariance=None
    ### ### ### ### ### ### ### ### ### ### ### ###
    lowcut=330
    xdata4fit, ydata4fit, ydata_err4fit = xdata[xdata>lowcut],ydata[xdata>lowcut],ydata_err[xdata>lowcut]
    ### ### ### ### ### ### ### ### ### ### ### ###    
    pfit, pfit_covariance = curve_fit(fitfunc, xdata4fit, ydata4fit , p0=p0
                                     ,bounds=bounds
                                     ,sigma=ydata_err4fit
                                     ,absolute_sigma=True)
    perr = np.sqrt(np.diag(pfit_covariance))
    ### ### ### ### ### ### ### ### ### ### ### ###
#    fitparams['value']=pfit
#    fitparams['abs.err.']=perr
#    fitparams['rel.err.']=np.abs(perr/pfit)
    ### ### ### ### ### ### ### ### ### ### ### ###
   
    yfit = fitfunc(xdata,*pfit)
    
    AreaRatio = pfit[0]/pfit[4]
    AreaRatio_err = np.sqrt((perr[0]/pfit[0])**2+(perr[4]/pfit[4])**2) * AreaRatio
    print("Ratio of Areas Peak1/Peak2 = {0:.3f} +/- {1:.3f}".format(AreaRatio,AreaRatio_err))
        
    
    arg0,arg1=0,0
    x=xdata
    y=0*x.copy()
    for peak in peaks:
        arg0=arg1
        arg1=arg0+peak['nargs']-1
        peak['xfit'] = x
        peak['yfit'] = peak['func'](peak['xfit'],*pfit[arg0:arg1])
        y+=peak['yfit']
        iarg=arg0
        for k in peak['params']:
            peak['params'][k]=pfit[iarg]
            iarg+=1
    yfitBG = BGfunc( y   , *pfit[-(BGnargs-1)::])
    BGparams['BGscale']=pfit[-2]   
    BGparams['Offset'] =pfit[-1]   
    
    
    fig, axes = plt.subplots(2, 2, figsize=(15, 18))
    
    ax=axes[0][0]
    #ax.plot(xdata,yinit,':',label='initial guess')
    ax.errorbar(xdata, ydata, yerr=ydata_err, fmt='.',color='black')
    ax.plot(xdata,yfitBG,':',label='background')
    ax.plot(xdata,yfit,'-r',label='best fit',linewidth=2)
    ax.plot(xdata,ydata,'.k',label='data')
    ax.plot(xdata,yfit-ydata,'-',label='residual')
    #ax.plot(xdata,ydata,'.')
    ax.set_title('Ag MNN '+k)
    ax.legend()
    
    ax=axes[0][1]
    ax.plot(xdata,yfitBG,':',label='background')
    num=0
    for peak in peaks:
        num+=1
        ax.plot(peak['xfit'],peak['yfit'],'-',label='peak '+str(num))
    #ax.plot(xdata,yinit,'--k',label='init')
    ax.set_title("Ratio of Areas Peak1/Peak2 = {0:.3f} +/- {1:.3f}".format(AreaRatio,AreaRatio_err))
    ax.legend()
    
    axes[1][0].set_axis_off()
    axes[1][1].set_axis_off()
    colwidth=20
   # txt=fitparams.T.to_string(float_format='%23.3e',col_space=colwidth,line_width=6*colwidth,justify='right')
#    axes[1][0].text(0,0.,txt,fontsize=12)
    
    plt.show()
    
    #print(fitparams.T)
#    print()
#    print()
    #'''
    #print(txt)
    
#endfor
