# -*- coding: utf-8 -*-
"""
@author: (c) torsten.leitner@gmail.com
"""    




### ### ### ### ### ### ### ### ### ### ### ### ### ###
### python libraries
### ### ### ### ### ### ### ### ### ### ### ### ### ###
import sys, os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt 
import tables

# own libs from pycoin folder one level above
#from libcoin.h5analyze_get_maps_from_multiple_h5files import h5analyze_get_maps_from_multiple_h5files
from libcoin.plot.plot_pcolor_map import plot_pcolor_map
#from libcoin.plot.plot_allinone_collage import plot_allinone_collage
from libcoin.plot.plot_all3maps_frames_in_true_only import plot_all3maps_frames_in_true_only

<<<<<<< Updated upstream
from libcoin.io.ASCcfg import ASCcfg_from_h5
# from libcoin.dfmap_from_table import dfmap_from_table
from libcoin.io.dfmaps_from_pytables_search import dfmaps_from_pytables_search
=======
from libcoin.ASCcfg import ASCcfg_from_h5
# from libcoin.dfmap_from_table import dfmap_from_table
from libcoin.dfmaps_from_pytables_search import dfmaps_from_pytables_search
>>>>>>> Stashed changes

### ### ### ### ### ### ### ### ### ### ### ### ### ###



### ### ### ### ### ### ### ### ### ### ### ### ### ###
### dataset descritption
### ### ### ### ### ### ### ### ### ### ### ### ### ###


#'''
picpath=basepath+'/pics'


h5name=os.path.join(basepath,datafilename)
# h5key for dataset
ASCcfg = ASCcfg_from_h5(h5name, h5path='/')

# histogram
#pictxt='_binnned'
histgram_method='weighted' 

# Axis params
par={}  
# A1: Auger specs
par['a1']={ 
      'name'      : 'Ag MNN'
     ,'type'      : 'Auger'
     ,'axis'      : 0
     ,'erange'    : ASCcfg['a1']['Erange']#[330,359]
     ,'EkinFermi' : EkinFermi_xy[0]
     ,'de'        : dxy[0]
     }
# A2: PES specs
par['a2']={ 
      'name'      : 'Ag 3d'
     ,'type'      : 'PES'
     ,'axis'      : 1
     ,'erange'    : ASCcfg['a2']['Erange']#[365,379]
     ,'EkinFermi' : EkinFermi_xy[1]
     ,'de'        : dxy[1]
     }
#par['prefix'] = prefix
par['picpath']= picpath
par['Ephot']  = 700

### ### ### ### ### ### ### ### ### ### ### ### ### ###



### ### ### ### ### ### ### ### ### ### ### ### ### ###
### plot parameter
### ### ### ### ### ### ### ### ### ### ### ### ### ###

fignum=0
nbinsSumDiff=31
ncontourlevels=25
cmap=plt.cm.get_cmap('terrain_r', ncontourlevels)

### ### ### ### ### ### ### ### ### ### ### ### ### ###







### ### ### ### ### ### ### ### ### ### ### ### ### ###
### Load Data with explicit database request
### ### ### ### ### ### ### ### ### ### ### ### ### ###
# unfortunately, one has to use kinetic energies here.

# histogram par
dsetkey='etp'
xvar='E1'
xrange=ASCcfg['a1']['Erange']
yvar='E2'
yrange=ASCcfg['a2']['Erange']

dfcoin,dfacc,dftrue = dfmaps_from_pytables_search( h5name, xrange, yrange, dxy, xvar='E1', yvar='E2', dsetkey=dsetkey)
# default: condition='( theta1 < 22*(3.1415/180) )'&'( theta2 < 22*(3.1415/180) )'

print('\n finished loading.')
### ### ### ### ### ### ### ### ### ### ### ### ### ###






### ### ### ### ### ### ### ### ### ### ### ### ### ###
### Calibrate and name datasets
### ### ### ### ### ### ### ### ### ### ### ### ### ###
dftrue.name='true'; dfacc.name='acc'; dfcoin.name='coin'; 
# Claibrate Energy axis with EkinFermi(a1;a2) and Ephot(mono)
for df in [dftrue,dfacc,dfcoin]:
    # Auger
    df.index  = df.index   + (par['Ephot']-par['a1']['EkinFermi'])  # Auger
    # PES
    df.columns= df.columns - (par['a2']['EkinFermi']) # PES  
### ### ### ### ### ### ### ### ### ### ### ### ### ###







#'''
#
