

from scipy.signal import savgol_filter
savgoldegree=3

picpostfix=''

#picpostfix='_allcoins'
#picpostfix='_singles_allcoins'
#picpostfix='_singles'
#picpostfix='_M4only'
#picpostfix='_M5only'
#picpostfix='_noM4M5sum'



### ### ### ### ### ### ### ### ### ### ### ### ### ###
### plot 1d specs
### ### ### ### ### ### ### ### ### ### ### ### ### ###
fig, axes = plt.subplots(1, 2, figsize=(15, 5), gridspec_kw = {'top':0.76} )

# draw A1 spec - Auger
key ='a1'
iax=0
#'''
# Singles from Acc
roi=singles; axes[iax].plot(scale_singles*roi[key],'.',color='k',markersize=markersize, marker='.',label='singles x {:.2f}'.format(scale_singles))
yy = savgol_filter(scale_singles*roi[key], savgolwitdh, savgoldegree); axes[iax].plot(roi[key].index, yy,':',color='k')
# All True
#'''
#'''
roi=fulltrue
axes[iax].plot(roi[key],'.',color='k',markersize=markersize, marker='^', label='all coin')
yy = savgol_filter(roi[key], savgolwitdh, savgoldegree)
axes[iax].plot(roi[key].index, yy,'--',color='k')
#'''
#''' cuts
for roi in ROIs: 
    if roi['txt'] in ['M4','M5']: #,'M5VV','M5VV left','M5VV right','M4VV','M4VV left','M4VV right','3d3/2to5/2']:
        sc=1 #max(roi[key].iloc[roi[key].index<-371])
        axes[iax].plot( roi[key]/sc,'.',color=roi['linecolor'], marker=roi['marker'], markersize=markersize,label=roi['txt']+'VV')
        yy = savgol_filter(roi[key], savgolwitdh, savgoldegree)
        axes[iax].plot(roi[key].index, yy,'-',color=roi['linecolor'])
        if roi['txt']=='M4':
            specm4vv=roi[key]/sc
            yy4vv  =yy
        if roi['txt']=='M5':
            specm5vv=roi[key]/sc
            yy5vv  =yy
#
'''
sumspec=specm4vv+specm5vv
axes[iax].plot(sumscale*sumspec,'.r',markersize=markersize,label='M4VV + M5VV', marker='v')            
yy = savgol_filter(sumspec, savgolwitdh, savgoldegree)
axes[iax].plot(sumspec.index, sumscale*yy,'-',color='red')
#'''
axes[iax].legend(loc='upper left')
axes[iax].set_xlabel(par[key]['name']+'  E (eV)')
axes[iax].set_ylabel('counts')
#


if flag_plot_two_hole_energy_axes:
    # PES peaks at: 373.8(3)eV & 367.8(4) => delta = 6 eV  
    # M4/5 two hole binding energy is given by M4/5_PE_energy - AugerEnergyAxis 
    ax0box=axes[iax].get_position()
    xmin,xmax=axes[iax].get_xbound()
    dypos=0.1
    # M5 
    Eshift=-367.8
    ax1 = fig.add_axes([ax0box.x0,ax0box.y0+ax0box.height+dypos,ax0box.width,0.0])
    ax1.set_yticklabels([''])
    ax1.set_xlabel('M5VV two-hole energy')
    ax1.set_xbound(xmin+Eshift,xmax+Eshift)
    # M4 
    Eshift=-373.8
    ax2 = fig.add_axes([ax0box.x0,ax0box.y0+ax0box.height+2*dypos,ax0box.width,0.0])
    ax2.set_yticklabels([''])
    ax2.set_xlabel('M4VV two-hole energy')
    ax2.set_xbound(xmin+Eshift,xmax+Eshift)




# draw A2 spec - PES
key ='a2'
iax=1
#'''
ec=singles ; sc = 1#np.max(ec[key].iloc[ec[key].index<-371])
axes[iax].plot(scale_singles*ec[key]/sc,':',color='k',label='singles x {:.2f}'.format(scale_singles))
#'''
#'''
ec=fulltrue; sc = 1#np.max(ec[key].iloc[ec[key].index<-371])
axes[iax].plot(ec[key]/sc,'--',color='k',label='all coin')
#'''
#'''
for ec in ROIs: 
#    if ec['txt'] in ['3d3/2','3d5/2','M5VV','M5VV left','M5VV right','M4VV', 'M4VV left','M4VV right','3d3/2+M5VV']:
    if ec['txt'] in ['M5','M4']:
        sc = 1#np.max(ec[key].iloc[ec[key].index<-371])
        axes[iax].plot( ec[key]/sc , '-',label=ec['txt'],color=ec['linecolor'], marker=ec['marker'], markersize=markersize )
#'''
axes[iax].legend(loc='upper center')
axes[iax].set_xlabel(par[key]['name']+'  E (eV)')
axes[iax].set_ylabel('counts')
axes[iax].set_xlim([-377,-365])



if flag_savefig==True:
    plt.savefig(os.path.join(picpath,picprefix)+"_specs_Auger_and_PES"+picpostfix+".pdf")
plt.show()
### ### ### ### ### ### ### ### ### ### ### ### ### ###


print('left: Auger, bin = {0:.3f} eV'.format(dxy[0])+' - right: PE, bin = {0:.3f} eV'.format(dxy[1]))

