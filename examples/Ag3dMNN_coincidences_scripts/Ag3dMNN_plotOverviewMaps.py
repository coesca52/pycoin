# -*- coding: utf-8 -*-
"""
@author: (c) torsten.leitner@gmail.com
"""    



#  Figure 4: measured, accidental, true coincidences maps
### ### ### ### ### ### ### ### ### ### ### ### ### ###
### # all 3 maps as horizontal subplot, Frames only in true map
### ### ### ### ### ### ### ### ### ### ### ### ### ###
plot_all3maps_frames_in_true_only(dfcoin,dfacc,dftrue
                    ,par ,cmap=cmap,fignum=77
                    ,flag_savefig=flag_save2dplot
                    ,picprefix=os.path.join(picpath,picprefix)
                    ,picpostfix=''
                    ,notitle=False
                    ,print_dxy=False
                    ,invertXaxis=False
                    ,invertYaxis=False
                    #,framecoords=ROIs,framelinestyle='--',framelinewidth=3
                    ,figsize=(18, 4)
                    )



# Figure X:  single maps, NO spectra
### ### ### ### ### ### ### ### ### ### ### ### ### ###
### plot 2d overview (single) maps with frames idicating the ROIs
### ### ### ### ### ### ### ### ### ### ### ### ### ###
if 0:
    # plot raw coin map
    if 1: 
        df=dfcoin ; figum=1 ; 
        ax1,fig1 = plot_pcolor_map(df,par,fignum=fignum,cmap=cmap
            ,flag_savefig=flag_save2dplot  ### saveing the figure makes it slow!!!
            ,picpostfix='',picprefix=os.path.join(picpath,picprefix)
            ,framecoords=None,framelinestyle='--',framelinewidth=2 )
    # plot acc coin map
    if 1: 
        df=dfacc ; figum=2 ; 
        ax2,fig2 = plot_pcolor_map(df,par,fignum=fignum,cmap=cmap
            ,flag_savefig=flag_save2dplot  ### saveing the figure makes it slow!!!
            ,picpostfix='',picprefix=os.path.join(picpath,picprefix)
            ,framecoords=None,framelinestyle='--',framelinewidth=2 )
    # plot true coin map
    if 1: 
        df=dftrue ; figum=3 ; 
        ax3,fig3 = plot_pcolor_map(df,par,fignum=fignum,cmap=cmap
            ,flag_savefig=flag_save2dplot  ### saveing the figure makes it slow!!!
            ,picpostfix='',picprefix=os.path.join(picpath,picprefix)
            ,framecoords=ROIs,framelinestyle='--',framelinewidth=2 )

        
        
# Figure X:  single maps, WITH spectra
### ### ### ### ### ### ### ### ### ### ### ### ### ###
### plot 2d overview (single) maps with frames idicating the ROIs
### ### ### ### ### ### ### ### ### ### ### ### ### ###
if 0:
    from libcoin.plot.plot_map_with_spectra import * #plot_map_with_specs

    # plot coin map with specs
    if 1:
        df=dfcoin
        ax7,fig7 = plot_map_with_spectra(df,par,cmap=cmap
            ,picpostfix='',picprefix=os.path.join(picpath,picprefix)
     #       , Ncolorticks=5, Nticks_topspec=3, Nticks_sidespec=4
            #, plot_maxtraces_in_map=True
            , framecoords=ROIs, framelinestyle='--',framelinewidth=1
            , plot_framesums=True
            , figsize=(6,6)
            , flag_savefig=flag_savefig
            , space_between_plots=0
            , linplt_sz = 0.4   , notitle=True
            , verPlotArea=[0.08,0.98] # (bottom,top)
            , horPlotArea=[0.12,0.94] # (left,right)
            ) 
    # plot acc map with specs
    if 0:
        df=dfacc
        ax8,fig8 = plot_map_with_spectra(df,par,cmap=cmap
            ,picpostfix='',picprefix=os.path.join(picpath,picprefix)
     #       , Ncolorticks=5, Nticks_topspec=3, Nticks_sidespec=4
            #, plot_maxtraces_in_map=True
            , framecoords=ROIs, framelinestyle='--',framelinewidth=1
            , plot_framesums=True
            , figsize=(6,6)
            , flag_savefig=flag_savefig
            , space_between_plots=0
            , linplt_sz = 0.4   , notitle=True
            , verPlotArea=[0.08,0.98] # (bottom,top)
            , horPlotArea=[0.12,0.94] # (left,right)
            ) 
        # plot true map with specs
    if 1:
        df=dftrue
        ax9,fig9 = plot_map_with_spectra(df,par,cmap=cmap
            ,picpostfix='',picprefix=os.path.join(picpath,picprefix)
     #       , Ncolorticks=5, Nticks_topspec=3, Nticks_sidespec=4
            #, plot_maxtraces_in_map=True
            , framecoords=ROIs, framelinestyle='--',framelinewidth=1
            , plot_framesums=True
            , figsize=(6,6)
            , flag_savefig=flag_savefig
            , space_between_plots=0
            , linplt_sz = 0.4   , notitle=True
            , verPlotArea=[0.08,0.98] # (bottom,top)
            , horPlotArea=[0.12,0.94] # (left,right)
            ) 
        
        
if 1:
    from libcoin.plot.plot_map_with_spectra import * #plot_map_with_specs

    # plot coin map with specs
    if 0:
        df=dfcoin
        ax7,fig7 = plot_map_with_spectra(df,par,cmap=cmap
            ,picpostfix='',picprefix=os.path.join(picpath,picprefix)
     #       , Ncolorticks=5, Nticks_topspec=3, Nticks_sidespec=4
            #, plot_maxtraces_in_map=True
            , framecoords=ROIs, framelinestyle='--',framelinewidth=1
            , plot_framesums=True
            , figsize=(6,6)
            , flag_savefig=flag_savefig
            , space_between_plots=0
            , linplt_sz = 0.4   , notitle=True
            , verPlotArea=[0.08,0.98] # (bottom,top)
            , horPlotArea=[0.12,0.94] # (left,right)
            ) 
    # plot acc map with specs
    if 0:
        df=dfacc
        ax8,fig8 = plot_map_with_spectra(df,par,cmap=cmap
            ,picpostfix='',picprefix=os.path.join(picpath,picprefix)
     #       , Ncolorticks=5, Nticks_topspec=3, Nticks_sidespec=4
            #, plot_maxtraces_in_map=True
            , framecoords=ROIs, framelinestyle='--',framelinewidth=1
            , plot_framesums=True
            , figsize=(6,6)
            , flag_savefig=flag_savefig
            , space_between_plots=0
            , linplt_sz = 0.4   , notitle=True
            , verPlotArea=[0.08,0.98] # (bottom,top)
            , horPlotArea=[0.12,0.94] # (left,right)
            ) 
        # plot true map with specs
    if 1:
        df=dftrue
        ax9,fig9 = plot_map_with_spectra(df,par,cmap=cmap
            ,picpostfix='',picprefix=os.path.join(picpath,picprefix)
     #       , Ncolorticks=5, Nticks_topspec=3, Nticks_sidespec=4
            #, plot_maxtraces_in_map=True
            , framecoords=ROIs, framelinestyle='-',framelinewidth=1
            , plot_framesums=True
            , figsize=(6,6)
            , flag_savefig=flag_savefig
            , space_between_plots=0
            , linplt_sz = 0.4   , notitle=True
            , verPlotArea=[0.08,0.98] # (bottom,top)
            , horPlotArea=[0.12,0.94] # (left,right)
            )         
        
        
        
    