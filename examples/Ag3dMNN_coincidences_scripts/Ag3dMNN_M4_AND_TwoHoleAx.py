
fig, axes = plt.subplots(1, 2, figsize=(15, 8),squeeze=False)

artof='a1'

### Fig. 6a: The M4 coincidence Auger spectrum AND scaled down M5 ANNd difference M4-M5
ax=axes[0][0]    
plotted={}
for roi in ROIs: 
    if roi['txt'] in ['M4','M5']:
        sc=1
        label=roi['txt']+'VV'
        if roi['txt']=='M4':
            label+=' measured'
        if roi['txt']=='M5':
            sc=M5scale
            label+=' x {0:.2f}'.format(sc)
        x  = roi[artof].index
        y  = roi[artof].values
        yy = savgol_filter(y, savgolwitdh, savgoldegree)
        ax.plot( x,sc*y ,'.',color=roi['linecolor'], marker=roi['marker'], markersize=markersize)
        ax.plot( x,sc*yy,'-',color=roi['linecolor'],label=label)
        plotted[roi['txt']]=pd.Series(data=y, index=x, name=roi['txt']+'VV')
if 1:
    M4clean=plotted['M4']-M5scale*plotted['M5']
    x  = M4clean.index
    y  = M4clean.values
    yy = savgol_filter(y, savgolwitdh, savgoldegree)
    ax.plot( x,y ,'.',color=M4cleancolor, marker='.', markersize=markersize)
    ax.plot( x,yy,'-',color=M4cleancolor,  label='M4VV clean')
ax.set_xlabel('Auger energy (eV)')
ax.set_ylabel('counts')
ax.legend()
ax.set_title('M4VV spectrum')


### Fig. 6b: The coincidence Auger spectra plotted with two-hole binding energy axis
# plots
ax=axes[0][1]    
#plotted={}
for roi in ROIs: 
    if roi['txt'] in ['M4','M5']:
        label=roi['txt']+'VV'
        if roi['txt']=='M4':
            label+=' measured'
        eshift=TwoHoleBindingEnergyOffset[roi['txt']]
        x  = roi[artof].index-eshift
        y  = roi[artof].values
        yy = savgol_filter(y, savgolwitdh, savgoldegree)
        ax.plot( x,y ,'.',color=roi['linecolor'], marker=roi['marker'], markersize=markersize)
        ax.plot(x, yy,'-',color=roi['linecolor'],label=label)
#        plotted[roi['txt']]=pd.Series(data=y, index=x, name=roi['txt']+'VV')
if 1:
    eshift=TwoHoleBindingEnergyOffset['M4']
    x  = M4clean.index-eshift
    y  = M4clean.values
    yy = savgol_filter(y, savgolwitdh, savgoldegree)
    ax.plot( x,y ,'.',color=M4cleancolor, marker='.', markersize=markersize)
    ax.plot(x, yy,'-',color=M4cleancolor,label='M4VV clean')

ax.legend(loc='upper left')
ax.set_xlabel('two-hole binding energy (eV)')
ax.set_ylabel('counts')
ax.set_title('Auger spectra on two-hole energy scale')



if flag_savefig==True:
    plt.savefig(os.path.join(picpath,picprefix)+"_specs_Auger_2holeEnergyAxis_AND_M4cleaning_scale"+
                str(int(np.round(100*M5scale)))+"pc.pdf")
plt.show()