
df=dftrue

# define Regions-of-Interest (ROIs) as frames in the map
ROIs=[]
a1maxrange=[df.index.min(),df.index.max()]
a2maxrange=[df.columns.min(),df.columns.max()]

ROIs.append( {'a1range':a1maxrange,  'a2range':[-369.,-366.5],   'txt':'M5'       ,'linecolor':'tab:blue','marker':'x'    } )
#ROIs.append( {'a1range':a1maxrange,  'a2range':[-372.0,-370],   'txt':'M4 to M5'       ,'linecolor':'tab:olive'  } )
ROIs.append( {'a1range':a1maxrange,  'a2range':[-375,-372.5],   'txt':'M4'       ,'linecolor':'tab:orange','marker':'+'  } )

#ROIs.append( {'a1range':[358.0,361.5],'a2range':a2maxrange, 'txt':'M4VV'        ,'linecolor':'tab:green'   } )
#ROIs.append( {'a1range':[352.0,356.0],'a2range':a2maxrange, 'txt':'M5VV'        ,'linecolor':'tab:red'     } )
#ROIs.append( {'a1range':[347.0,351.5],'a2range':a2maxrange, 'txt':'preM5VV'        ,'linecolor':'tab:purple'     } )





# calc stuff for each ROIs
for roi in ROIs:
    tmpdf=df.loc[(roi['a1range'][0]<=df.index)  &(df.index  <=roi['a1range'][1])
                ,(roi['a2range'][0]<=df.columns)&(df.columns<=roi['a2range'][1])]
    roi['a1']=(tmpdf.sum(axis=1))
    roi['a2']=(tmpdf.sum(axis=0))
### ### ### ### ### ###

# Singles from Acc counts
singles  = { 'a1':(dfacc.sum(axis=1))
            ,'a2':(dfacc.sum(axis=0)) }
# All True Coins
fulltrue = { 'a1':(dftrue.sum(axis=1) )
            ,'a2':(dftrue.sum(axis=0) )   }