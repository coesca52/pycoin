
### ### ### ### ### ### ### ### ### ### ### ###
from scipy.special import wofz
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt 
import pandas as pd
### ### ### ### ### ### ### ### ### ### ### ###
def VoigtProfile(x, area, x0, alpha, gamma):
    """
    Return the Voigt line shape at x with Lorentzian component HWHM gamma
    and Gaussian component HWHM alpha.
    The area of the profile is scaled to match the parameter 'area'.
    uses: from scipy.special import wofz
    """
    #from scipy.special import wofz
    sigma = alpha / np.sqrt(2 * np.log(2))         
    out   = np.real(wofz(( (x-x0) + 1j*gamma)/sigma/np.sqrt(2))) / sigma /np.sqrt(2*np.pi)
    out  *= area
    return out
### ### ### ### ### ### ### ### ### ### ### ###



### ### ### ### ### ### ### ### ### ### ### ###
def funcShirleyBG(y,BGscale,Offset):
    out  = y[::-1].cumsum()[::-1]  #reverse cumsum 
    out -= out.min()
    out /= out.max()
    return BGscale*out + Offset
### ### ### ### ### ### ### ### ### ### ### ###
def guessShirleyBG(y,edgesize=5):
    left = y[0:edgesize-1].mean()
    right= y[-edgesize:-1].mean()
    #
    Offset = min(left,right)
    BGscale= max(left,right) - Offset
    return BGscale,Offset
### ### ### ### ### ### ### ### ### ### ### ###



try:
    label_peak1
except:
    label_peak1='peak 1'
try:
    label_peak2
except:
    label_peak2='peak 2'



figlist=[]
axlist=[]
for k in datdict:
    print(k)
    
    dat=datdict[k]
    artof='a2'
    ### ### ### ### ### ### ### ### ### ### ### ###
    xdata = np.array(dat[artof].index)
    ydata = np.array(dat[artof].values)
    ydata_err = np.sqrt(abs(ydata))
    #plt.plot(xdata,ydata);plt.show()
    # initial guess params
    peak1_x0 = peak1_pos 
    peak2_x0 = peak2_pos
    area     = .5*ydata.sum()*(xdata[1]-xdata[0])
    alpha    = .2
    gamma    = .2
    BGscale,Offset = guessShirleyBG(ydata) #; print(BGscale,Offset)
    ### ### ### ### ### ### ### ### ### ### ### ###



    ### ### ### ### ### ### ### ### ### ### ### ###
    # parts of fit model
    peak1func   = VoigtProfile      #(x, x0, amp, alpha, gamma)
    peak1params = { 'area':area , 'x0': peak1_x0 , 'alpha':alpha , 'gamma':gamma} ; 
    peak1bounds = { 'area':[-np.inf,np.inf] , 'x0': [-np.inf,np.inf] , 'alpha':[0,np.inf] , 'gamma':[0,np.inf]}
    peak1params = {'peak1_'+k: v for k, v in peak1params.items()}
    peak1bounds = {'peak1_'+k: v for k, v in peak1bounds.items()}
    #
    peak2func   = VoigtProfile      #(x, x0, amp, alpha, gamma)
    peak2params = { 'area':area , 'x0': peak2_x0 , 'alpha':alpha , 'gamma':gamma}
    peak2bounds = { 'area':[-np.inf,np.inf] , 'x0': [-np.inf,np.inf] ,'alpha':[0,np.inf] , 'gamma':[0,np.inf]}
    peak2params = {'peak2_'+k: v for k, v in peak2params.items()}
    peak2bounds = {'peak2_'+k: v for k, v in peak2bounds.items()}
    #
    # BG
    BGfunc   = funcShirleyBG        #(y,BGscale,Offset)
    BGparams = { 'BGscale':BGscale    , 'Offset':Offset }
    BGbounds = { 'BGscale':[-np.inf,np.inf] , 'Offset':[0,np.inf] }
    ### ### ### ### ### ### ### ### ### ### ### ###
    init      ={**peak1params, **peak2params, **BGparams}
    boundaries={**peak1bounds, **peak2bounds, **BGbounds}
    fitparams = pd.DataFrame.from_dict(init, orient='index', dtype=None, columns=['init'])
    fitparams['lbound']=-np.inf
    fitparams['ubound']= np.inf
    for kk in boundaries:
        a=boundaries[kk][0]
        b=boundaries[kk][1]
        #print(kk,a,b)
        fitparams.loc[kk,['lbound']]=a
        fitparams.loc[kk,['ubound']]=b
    ### ### ### ### ### ### ### ### ### ### ### ###

    ### ### ### ### ### ### ### ### ### ### ### ###
    def fitfunc(x,*args):
        """
        fit func with 2 Voigt profiles and constant background
        """
        y = peak1func(x, *args[0:4])
        y+= peak2func(x, *args[4:8])
        y+= BGfunc(y   , *args[-2::])
        return y
    ### ### ### ### ### ### ### ### ### ### ### ###

    # put together p0
    p0 = []
    for v in init.values():     
        p0.append(v)
    # put together bounds
    bounds = ( [] , [] )
    for v in boundaries.values():
        bounds[0].append(v[0])
        bounds[1].append(v[1])
    
    #print(bounds)
    
    yinit=fitfunc(xdata,*p0)
    pfit=None
    pfit_covariance=None
    #pfit, pfit_covariance = curve_fit(fitfunc, xdata, ydata, p0=p0, bounds=bounds)
    pfit, pfit_covariance = curve_fit(fitfunc, xdata, ydata, p0=p0
                                     ,bounds=bounds
                                     ,sigma=ydata_err
                                     ,absolute_sigma=True)
    perr = np.sqrt(np.diag(pfit_covariance))
    fitparams['value']=pfit
    fitparams['abs.err.']=perr
    fitparams['rel.err.']=np.abs(perr/pfit)
    
    yfit = fitfunc(xdata,*pfit)
    yfitpeak1 = peak1func(xdata, *pfit[0:4])
    yfitpeak2 = peak2func(xdata, *pfit[4:8])
    yfitBG    = BGfunc(yfitpeak1+yfitpeak2 , *pfit[-2::])
    AreaRatio = pfit[4]/pfit[0]
    AreaRatio_err = np.sqrt((perr[0]/pfit[0])**2+(perr[4]/pfit[4])**2) * AreaRatio
    print("Ratio of Areas Peak2/Peak1 = {0:.3f} +/- {1:.3f}".format(AreaRatio,AreaRatio_err))
    
    datdict[k][artof].fitfunc  =fitfunc
    datdict[k][artof].peakfunc =peak1func
    datdict[k][artof].BGfunc   =BGfunc
    datdict[k][artof].pfit     =pfit
    datdict[k][artof].fitresult=fitparams
    datdict[k][artof].fitresult.AreaRatio = AreaRatio
    datdict[k][artof].fitresult.AreaRatio_err = AreaRatio_err
    
    
    
    
    if flag_show_plots:
        if flag_show_fitparams:
            fig, axes = plt.subplots(2, 2, figsize=(15, 10), squeeze=False)
        else:
            fig, axes = plt.subplots(1, 2, figsize=(15,  5), squeeze=False)
    
    ax=axes[0][0]
    #ax.plot(xdata,yinit,':',label='initial guess')
    ax.errorbar(xdata, ydata, yerr=ydata_err, fmt='.',color='black')
    ax.plot(xdata,ydata,'.k',label='data')
    ax.plot(xdata,yfitBG,'-',label='background',color='tab:gray')
    ax.plot(xdata,yfit,'-k',label='best fit',linewidth=1)
    ax.plot(xdata,ydata-yfit,'--',label='residual',color='black')
    #ax.plot(xdata,ydata,'.')
    ax.set_title('Ag 3d '+k)
    ax.set_xlabel('Eb (eV)')
    ax.set_ylabel('counts')
    #ax.grid(True, color='tab:gray', linestyle='--')
    ax.legend()
    
    ax=axes[0][1]
    ax.plot(xdata,yfitBG,'-',label='background',color='tab:gray')
    ax.plot(xdata,yfitpeak1,'-',label=label_peak1,color='tab:orange')
    ax.plot(xdata,yfitpeak2,'-',label=label_peak2,color='tab:blue')
    ax.plot(xdata,yfit,':k',label='best fit',linewidth=1)
    ax.set_title("Ratio of Areas Peak1/Peak2 = {0:.3f} +/- {1:.3f}".format(AreaRatio,AreaRatio_err))
    ax.set_xlabel('Eb (eV)')
    ax.set_ylabel('counts')
    #ax.grid(True, color='tab:gray', linestyle='--')
    ax.legend()
    
    """
    txt="{}".format('          ')
    for idx in fitparams.index:    txt+=idx
    txt+='.\n'
    for col in fitparams.columns:
        txt+=col
        for idx in fitparams.index: txt+="{0:7.3f}".format(1)
        txt+='.\n'
    txt.expandtabs(2)
    colwidth=20
    #"""
    
    myinf = 1e32
    fitparams[fitparams==-np.inf]=-myinf
    fitparams[fitparams==+np.inf]=+myinf
    '''
    txt = fitparams.T.to_string( float_format='%20.3e', col_space=colwidth, line_width=6*colwidth, justify='left' )
    axes[1][0].text(0,0,txt,fontsize=11)
    #'''

    if flag_show_fitparams:
        # Add parameter tables in new subplots
        ax=plt.subplot(3, 1, 3)
        ax.set_axis_off()

        tabdata=fitparams.iloc[0:4]
        cols=[];rows=[];cell_text=[]
        for col in tabdata.T.columns:    cols.append(col)
        for row in tabdata.T.index:  rows.append(row)
        for valuerow in tabdata.T.values: cell_text.append(['%5.5g' % val for val in valuerow])
        ax.table(cellText=cell_text ,rowLabels=rows ,colLabels=cols ,loc='top') 

        tabdata=fitparams.iloc[4:8]
        cols=[];rows=[];cell_text=[]
        for col in tabdata.T.columns:    cols.append(col)
        for row in tabdata.T.index:  rows.append(row)
        for valuerow in tabdata.T.values: cell_text.append(['%5.5g' % val for val in valuerow])
        ax.table(cellText=cell_text ,rowLabels=rows ,colLabels=cols ,loc='center') 

        tabdata=fitparams.iloc[8::]
        cols=[];rows=[];cell_text=[]
        for col in tabdata.T.columns:    cols.append(col)
        for row in tabdata.T.index:  rows.append(row)
        for valuerow in tabdata.T.values: cell_text.append(['%5.5g' % val for val in valuerow])
        ax.table(cellText=cell_text ,rowLabels=rows ,colLabels=cols ,loc='bottom') 

        # Adjust layout to make room for the table:
        plt.subplots_adjust(left=0.08 , bottom=0.15)
    
    if flag_savefig:
        picname = os.path.join(picpath,picprefix+'_'+'FitPES_'+k+'.pdf')
        plt.savefig(picname,orientation='portrait')
    
    
    plt.show()
    
#endfor


#### post fit evaluation

if flag_estimate_M5inM4:
    ### height of BG from peak1: M5=3d5/2 under peak2: M4 (3d3/2)
    artof='a2' #PES
    dataname='all_coins'; 
    data=datdict[dataname][artof]
    fitresult=datdict[dataname][artof]
    xfit=np.linspace(-378,-366,100)
    fit      = pd.Series(data=fitresult.fitfunc (xfit,*fitresult.pfit    ),index=xfit)
    fitpeak1 = pd.Series(data=fitresult.peakfunc(xfit,*fitresult.pfit[0:4]),index=xfit)
    fitpeak2 = pd.Series(data=fitresult.peakfunc(xfit,*fitresult.pfit[4:8]),index=xfit)
    fitBG    = pd.Series(data=fitresult.BGfunc  (fitpeak1+fitpeak2,*fitresult.pfit[-2::]),index=xfit)

    x1=-376.0 ;  x2=-372.0
    peak=fit[(x1<fit.index)&(fit.index<x2)].max()
    x1=-371.0 ;  x2=-370.5
    BG  =fit[(x1<fit.index)&(fit.index<x2)].mean()
    scale=peak/BG

    print("M4 PES Height: {:.1f} , BG between M4&M5: {:.1f}\n ==> M5 content in M4: M5scale = M4height/BG = {:.2f}".format(peak,BG,scale) )

zoomRelpeakmax=.3
if flag_plot_BGzoom:
    fig, ax = plt.subplots(1,1, figsize=(11, 4), squeeze=True)
    ax.plot(data,'.k')
    ax.plot(fit,'-k',label='best fit')
    ax.plot(fitBG,'-',label='background',color='tab:gray')
    ax.plot(fitpeak1,'-',label='M4',color='tab:blue')
    ax.plot(fitpeak2,'-',label='M5',color='tab:orange')
    ax.plot(xdata,ydata-yfit,'--',label='residual',color='black')
    x=xdata
    y=8*(xdata+370)+95
    ax.plot(x,y,'-.',label='line',color='green')
    #ax.xlim([-376,-368])
    ax.set_ylim([ax.get_ylim()[0],zoomRelpeakmax*ax.get_ylim()[1]])
    ax.legend()
    if flag_savefig:
        picname = os.path.join(picpath,picprefix+'_'+'FitPES_'+k+'_zoomBG.pdf')
        plt.savefig(picname,orientation='portrait')
    plt.show()


print()



