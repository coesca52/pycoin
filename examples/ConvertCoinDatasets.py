#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 19 03:52:37 2019

@author: (c) torsten.leitner@gmail.com
"""
import os
import sys

# own libs from pycoin folder one level above
pycoinpath='../'
sys.path.append(pycoinpath) # add libcoin path here (if not installed as module)


import libcoin
from libcoin.io.rawcoindataset_to_h5 import multiple_rawcoindatasets_to_h5


#'''
###############################################################################
###############################################################################
#basepath='D:\\DATA\\2019\\KW45_MB\\Coin'
basepath        = '../testdata'
datafolderbase  = 'Ag3dMNN_coin_data'
fnamebase       = 'data_'
###############################################################################
###############################################################################



###############################################################################
# Can be replaced by QFileDialog giving back list of folders
###############################################################################
datafolderlist=[]
firstfoldernum,lastfoldernum = 1,2 # _<num> for individual coin runs. scriopt is ready for mass conversion
for datafoldernum in range(firstfoldernum,lastfoldernum+1) :
    datafolder=datafolderbase+'_'+str(int(datafoldernum)).zfill(0)
    datafolderlist.append( os.path.join(basepath,datafolder) )
###############################################################################




###############################################################################
# load and convert multiple datasets:
multiple_rawcoindatasets_to_h5(datafolderlist,fnamebase
#                ,startiteration=1,stopiteration=8
                )
###############################################################################

#'''


